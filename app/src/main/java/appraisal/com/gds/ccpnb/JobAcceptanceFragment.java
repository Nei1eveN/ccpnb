package appraisal.com.gds.ccpnb;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import appraisal.com.gds.ccpnb.adapters.JobAcceptanceAdapter;
import appraisal.com.gds.ccpnb.presenters.records.pending.JobAcceptancePresenter;
import appraisal.com.gds.ccpnb.presenters.records.pending.JobPresenterImpl;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static appraisal.com.gds.ccpnb.utils.Constants.KEY_USER_EMAIL;


/**
 * A simple {@link Fragment} subclass.
 */
public class JobAcceptanceFragment extends Fragment implements JobAcceptancePresenter.View, SwipeRefreshLayout.OnRefreshListener {

    private Unbinder unbinder;
    @BindView(R.id.coorList)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.swipeList)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.ivEmptyList)
    ImageView emptyImage;
    @BindView(R.id.tvEmptyList)
    TextView emptyText;
    @BindView(R.id.rvList)
    RecyclerView recyclerView;

    private String email;

    private ProgressDialog progressDialog;

    private JobAcceptancePresenter presenter;

    private boolean hasLoadedOnce = false;

    public JobAcceptanceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new JobPresenterImpl(this, getActivity());
        Intent intent = Objects.requireNonNull(getActivity()).getIntent();
        email = intent.getStringExtra(KEY_USER_EMAIL);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_job_acceptance, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        swipeRefreshLayout.setOnRefreshListener(this);

        progressDialog = new ProgressDialog(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart(email);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (isVisibleToUser && hasLoadedOnce) {
                presenter.onStart(email);
                hasLoadedOnce = true;
            }
        }
    }

    @Override
    public void onRefresh() {
        presenter.requestRecords(email);
    }

    @Override
    public void showProgress(String title, String message) {
        swipeRefreshLayout.setRefreshing(true);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
        progressDialog.hide();
    }

    @Override
    public void setList(JobAcceptanceAdapter adapter) {
        emptyImage.setVisibility(View.GONE);
        emptyText.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setEmptyState(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
        recyclerView.setVisibility(View.GONE);

        emptyImage.setVisibility(View.VISIBLE);
        emptyText.setVisibility(View.VISIBLE);

        emptyText.setText(message);
    }
}
