package appraisal.com.gds.ccpnb.presenters.login;

import android.content.Intent;

public interface LoginPresenter {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showErrorDialog(String title, String message);
        void showUserPageDialog(String title, String message, Intent userData);
    }
    void onDestroy();
    void submitLoginCredentials(String email, String password, String userType);
}
