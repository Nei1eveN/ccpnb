package appraisal.com.gds.ccpnb.presenters;

import appraisal.com.gds.ccpnb.adapters.JobAcceptanceDetailAdapter;

public interface PendingDetailInteractor {
    interface DetailsListener {
        void onDetailSuccess(JobAcceptanceDetailAdapter adapter);
        void onDetailError(String errorMessage);
    }
    void getDetails(String record_id, DetailsListener listener);
    void getOfflineData(String record_id, DetailsListener listener);
}
