package appraisal.com.gds.ccpnb.presenters.splash;

import android.content.Intent;

public interface SplashInteractor {
    interface FindUserListener {
        void onUserFound(String message, Intent resultData);
        void onUserNotFound(String message, Intent resultData);
    }
    void getUser(FindUserListener listener);
}
