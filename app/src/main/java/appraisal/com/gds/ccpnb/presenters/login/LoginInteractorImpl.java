package appraisal.com.gds.ccpnb.presenters.login;

import android.content.Context;
import android.content.Intent;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONObject;

import appraisal.com.gds.ccpnb.AppraiserHomeActivity;
import appraisal.com.gds.ccpnb.utils.SessionManager;

import static appraisal.com.gds.ccpnb.utils.Constants.EMAIL_PARAM;
import static appraisal.com.gds.ccpnb.utils.Constants.KEY_USER_EMAIL;
import static appraisal.com.gds.ccpnb.utils.Constants.KEY_USER_ROLE;
import static appraisal.com.gds.ccpnb.utils.Constants.PASSWORD_PARAM;
import static appraisal.com.gds.ccpnb.utils.Constants.PNB_URL_SIGN_IN;

class LoginInteractorImpl implements LoginInteractor {

    private Context context;

    LoginInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getLoginCredentials(final String email, String password, final String userType, final LoginListener listener) {
        AndroidNetworking.post(PNB_URL_SIGN_IN)
                .addBodyParameter(EMAIL_PARAM, email)
                .addBodyParameter(PASSWORD_PARAM, password)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        SessionManager sessionManager = new SessionManager(context);
                        sessionManager.setLogin(true, email, userType);
                        Intent userData = new Intent(context, AppraiserHomeActivity.class);
                        userData.putExtra(KEY_USER_EMAIL, email);
                        userData.putExtra(KEY_USER_ROLE, userType);
                        listener.onLoginSuccess("Login Successful", "Successfully Logged in as "+email, userData);

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        listener.onLoginFailure("Login Error", error.getMessage());
                    }
                });
    }
}
