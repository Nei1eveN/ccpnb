package appraisal.com.gds.ccpnb.presenters.records.pending;

import android.content.Context;

import appraisal.com.gds.ccpnb.adapters.JobAcceptanceAdapter;

public class JobPresenterImpl implements JobAcceptancePresenter, JobAcceptanceInteractor.RecordsListener {
    private JobAcceptancePresenter.View view;
    private JobAcceptanceInteractor interactor;

    public JobPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new JobAcceptanceInteractorImpl(context);
    }

    @Override
    public void onStart(String email) {
        if (view != null) {
            view.showProgress("Loading Pending Appraisals", "Please wait...");
        }
        interactor.getOfflineData(email, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestRecords(String email) {
        if (view != null) {
            view.showProgress("Fetching Data", "Please wait...");
        }
        interactor.getRecords(email, this);
    }

    @Override
    public void onListSuccess(JobAcceptanceAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.setList(adapter);
        }
    }

    @Override
    public void onListFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
        }
    }
}
