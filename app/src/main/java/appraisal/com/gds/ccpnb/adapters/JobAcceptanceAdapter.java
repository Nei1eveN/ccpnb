package appraisal.com.gds.ccpnb.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import appraisal.com.gds.ccpnb.JobAcceptanceDetailActivity;
import appraisal.com.gds.ccpnb.R;
import appraisal.com.gds.ccpnb.models.JobAcceptanceRecord;
import butterknife.BindView;
import butterknife.ButterKnife;

import static appraisal.com.gds.ccpnb.utils.Constants.RECORD_ID;

public class JobAcceptanceAdapter extends RecyclerView.Adapter<JobAcceptanceAdapter.AcceptanceViewHolder> {
    private Context context;
    private List<JobAcceptanceRecord> jobAcceptanceRecords;

    public JobAcceptanceAdapter(Context context, List<JobAcceptanceRecord> jobAcceptanceRecords) {
        this.context = context;
        this.jobAcceptanceRecords = jobAcceptanceRecords;
    }

    static class AcceptanceViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.constraintLayout)
        ConstraintLayout constraintLayout;

        @BindView(R.id.tvRecordName)
        TextView accountName;
        @BindView(R.id.tvApprType)
        TextView appraisalType;

        AcceptanceViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public AcceptanceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AcceptanceViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_job_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AcceptanceViewHolder holder, int position) {
        JobAcceptanceRecord record = jobAcceptanceRecords.get(position);

        holder.accountName.setText(String.format("%s, %s %s", record.getAppAccountLastName(), record.getAppAccountFirstName(), record.getAppAccountMiddleName()));
        holder.appraisalType.setText(record.getAppRequestAppraisal());
        holder.constraintLayout.setOnClickListener(view -> {
            Intent intent = new Intent(context, JobAcceptanceDetailActivity.class);
            intent.putExtra(RECORD_ID, record.getRecord_id());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return jobAcceptanceRecords.size();
    }
}
