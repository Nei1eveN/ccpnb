package appraisal.com.gds.ccpnb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import appraisal.com.gds.ccpnb.adapters.fragment_adapter.ViewPagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

public class AppraiserHomeActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appraiser_home);
        ButterKnife.bind(this);

        toolbar.setTitle("For Acceptance");
        toolbar.setSubtitle("Pending Appraisals");

        adapter.addFragment(new JobAcceptanceFragment(), "FOR ACCEPTANCE");
        adapter.addFragment(new ReworkAppraisalFragment(), "FOR REWORK");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setCurrentItem(0, false);
        viewPager.addOnPageChangeListener(this);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                toolbar.setTitle("For Acceptance");
                toolbar.setSubtitle("Pending Appraisals");
                break;
            case 1:
                toolbar.setTitle("For Rework");
                toolbar.setSubtitle("Subject For Revisions");
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
