package appraisal.com.gds.ccpnb.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import static appraisal.com.gds.ccpnb.utils.Constants.KEY_IS_LOGGEDIN;
import static appraisal.com.gds.ccpnb.utils.Constants.KEY_USER_EMAIL;
import static appraisal.com.gds.ccpnb.utils.Constants.KEY_USER_ROLE;
import static appraisal.com.gds.ccpnb.utils.Constants.PREF_NAME;

public class SessionManager {

    private static String TAG = SessionManager.class.getSimpleName();

    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        editor = sharedPreferences.edit();
    }

    public void setLogin(boolean isLoggedIn, String email, String user_role) {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        editor.putString(KEY_USER_EMAIL, email);
        editor.putString(KEY_USER_ROLE, user_role);
        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified.");
    }

    public String getUserRole() {
        return sharedPreferences.getString(KEY_USER_ROLE, "");
    }

    public String getUserEmail() {
        return sharedPreferences.getString(KEY_USER_EMAIL, "");
    }

    public boolean isLoggedIn(){
        return sharedPreferences.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    //Logout user
    public void LogOut() {
        editor.clear();
        editor.commit();
    }
}
