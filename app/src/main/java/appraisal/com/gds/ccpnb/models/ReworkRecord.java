package appraisal.com.gds.ccpnb.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ReworkRecord extends RealmObject {

    /**SYSTEM**/
    @PrimaryKey
    @SerializedName("record_id")
    @Expose
    private String record_id;

    /**RECORDS**/
    @SerializedName("app_account_first_name")
    @Expose
    private String appAccountFirstName;
    @SerializedName("app_account_last_name")
    @Expose
    private String appAccountLastName;
    @SerializedName("app_account_middle_name")
    @Expose
    private String appAccountMiddleName;

    /**APPRAISAL REQUEST**/
    @SerializedName("app_priority")
    @Expose
    private String appPriority;
    @SerializedName("app_request_appraisal")
    @Expose
    private String appRequestAppraisal;

    @SerializedName("application_status")
    @Expose
    private String applicationStatus;

    public String getRecord_id() {
        return record_id;
    }

    public void setRecord_id(String record_id) {
        this.record_id = record_id;
    }

    public String getAppAccountFirstName() {
        return appAccountFirstName;
    }

    public void setAppAccountFirstName(String appAccountFirstName) {
        this.appAccountFirstName = appAccountFirstName;
    }

    public String getAppAccountLastName() {
        return appAccountLastName;
    }

    public void setAppAccountLastName(String appAccountLastName) {
        this.appAccountLastName = appAccountLastName;
    }

    public String getAppAccountMiddleName() {
        return appAccountMiddleName;
    }

    public void setAppAccountMiddleName(String appAccountMiddleName) {
        this.appAccountMiddleName = appAccountMiddleName;
    }

    public String getAppPriority() {
        return appPriority;
    }

    public void setAppPriority(String appPriority) {
        this.appPriority = appPriority;
    }

    public String getAppRequestAppraisal() {
        return appRequestAppraisal;
    }

    public void setAppRequestAppraisal(String appRequestAppraisal) {
        this.appRequestAppraisal = appRequestAppraisal;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }
}
