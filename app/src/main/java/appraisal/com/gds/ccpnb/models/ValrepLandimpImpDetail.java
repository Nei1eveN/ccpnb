package appraisal.com.gds.ccpnb.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValrepLandimpImpDetail {

    @SerializedName("valrep_landimp_desc_beams")
    @Expose
    private String valrepLandimpDescBeams;
    @SerializedName("valrep_landimp_desc_category_class")
    @Expose
    private String valrepLandimpDescCategoryClass;
    @SerializedName("valrep_landimp_desc_ceiling")
    @Expose
    private String valrepLandimpDescCeiling;
    @SerializedName("valrep_landimp_desc_columns_posts")
    @Expose
    private String valrepLandimpDescColumnsPosts;
    @SerializedName("valrep_landimp_desc_compartments")
    @Expose
    private String valrepLandimpDescCompartments;
    @SerializedName("valrep_landimp_desc_confirmed_thru")
    @Expose
    private String valrepLandimpDescConfirmedThru;
    @SerializedName("valrep_landimp_desc_construction_feature")
    @Expose
    private String valrepLandimpDescConstructionFeature;
    @SerializedName("valrep_landimp_desc_doors")
    @Expose
    private String valrepLandimpDescDoors;
    @SerializedName("valrep_landimp_desc_economic_life")
    @Expose
    private String valrepLandimpDescEconomicLife;
    @SerializedName("valrep_landimp_desc_effective_age")
    @Expose
    private String valrepLandimpDescEffectiveAge;
    @SerializedName("valrep_landimp_desc_est_life")
    @Expose
    private String valrepLandimpDescEstLife;
    @SerializedName("valrep_landimp_desc_exterior_walls")
    @Expose
    private String valrepLandimpDescExteriorWalls;
    @SerializedName("valrep_landimp_desc_features_2f")
    @Expose
    private String valrepLandimpDescFeatures2f;
    @SerializedName("valrep_landimp_desc_features_3f")
    @Expose
    private String valrepLandimpDescFeatures3f;
    @SerializedName("valrep_landimp_desc_features_4f")
    @Expose
    private String valrepLandimpDescFeatures4f;
    @SerializedName("valrep_landimp_desc_features_gf")
    @Expose
    private String valrepLandimpDescFeaturesGf;
    @SerializedName("valrep_landimp_desc_foundation")
    @Expose
    private String valrepLandimpDescFoundation;
    @SerializedName("valrep_landimp_desc_imp_cost_grade")
    @Expose
    private String valrepLandimpDescImpCostGrade;
    @SerializedName("valrep_landimp_desc_imp_desc")
    @Expose
    private String valrepLandimpDescImpDesc;
    @SerializedName("valrep_landimp_desc_imp_fence")
    @Expose
    private String valrepLandimpDescImpFence;
    @SerializedName("valrep_landimp_desc_imp_floor_area")
    @Expose
    private String valrepLandimpDescImpFloorArea;
    @SerializedName("valrep_landimp_desc_imp_flooring")
    @Expose
    private String valrepLandimpDescImpFlooring;
    @SerializedName("valrep_landimp_desc_imp_framing")
    @Expose
    private String valrepLandimpDescImpFraming;
    @SerializedName("valrep_landimp_desc_imp_garage")
    @Expose
    private String valrepLandimpDescImpGarage;
    @SerializedName("valrep_landimp_desc_imp_no_storey")
    @Expose
    private String valrepLandimpDescImpNoStorey;
    @SerializedName("valrep_landimp_desc_imp_others")
    @Expose
    private String valrepLandimpDescImpOthers;
    @SerializedName("valrep_landimp_desc_imp_remain_life")
    @Expose
    private String valrepLandimpDescImpRemainLife;
    @SerializedName("valrep_landimp_desc_imp_roofing")
    @Expose
    private String valrepLandimpDescImpRoofing;
    @SerializedName("valrep_landimp_desc_imp_rooms")
    @Expose
    private String valrepLandimpDescImpRooms;
    @SerializedName("valrep_landimp_desc_imp_t_and_b")
    @Expose
    private String valrepLandimpDescImpTAndB;
    @SerializedName("valrep_landimp_desc_imp_use")
    @Expose
    private String valrepLandimpDescImpUse;
    @SerializedName("valrep_landimp_desc_imp_walls")
    @Expose
    private String valrepLandimpDescImpWalls;
    @SerializedName("valrep_landimp_desc_imp_windows")
    @Expose
    private String valrepLandimpDescImpWindows;
    @SerializedName("valrep_landimp_desc_interior_walls")
    @Expose
    private String valrepLandimpDescInteriorWalls;
    @SerializedName("valrep_landimp_desc_kitchen")
    @Expose
    private String valrepLandimpDescKitchen;
    @SerializedName("valrep_landimp_desc_observed_condition")
    @Expose
    private String valrepLandimpDescObservedCondition;
    @SerializedName("valrep_landimp_desc_occupants")
    @Expose
    private String valrepLandimpDescOccupants;
    @SerializedName("valrep_landimp_desc_owned_or_leased")
    @Expose
    private String valrepLandimpDescOwnedOrLeased;
    @SerializedName("valrep_landimp_desc_partition")
    @Expose
    private String valrepLandimpDescPartition;
    @SerializedName("valrep_landimp_desc_partitions")
    @Expose
    private String valrepLandimpDescPartitions;
    @SerializedName("valrep_landimp_desc_percent_completed")
    @Expose
    private String valrepLandimpDescPercentCompleted;
    @SerializedName("valrep_landimp_desc_property_type")
    @Expose
    private String valrepLandimpDescPropertyType;
    @SerializedName("valrep_landimp_desc_roof")
    @Expose
    private String valrepLandimpDescRoof;
    @SerializedName("valrep_landimp_desc_stairs")
    @Expose
    private String valrepLandimpDescStairs;
    @SerializedName("valrep_landimp_desc_tb")
    @Expose
    private String valrepLandimpDescTb;
    @SerializedName("valrep_landimp_desc_trusses")
    @Expose
    private String valrepLandimpDescTrusses;
    @SerializedName("valrep_landimp_desc_year_built")
    @Expose
    private String valrepLandimpDescYearBuilt;
    @SerializedName("valrep_landimp_impsummary1_actual_utilization")
    @Expose
    private String valrepLandimpImpsummary1ActualUtilization;
    @SerializedName("valrep_landimp_impsummary1_building_desc")
    @Expose
    private String valrepLandimpImpsummary1BuildingDesc;
    @SerializedName("valrep_landimp_impsummary1_erected_on_lot")
    @Expose
    private String valrepLandimpImpsummary1ErectedOnLot;
    @SerializedName("valrep_landimp_impsummary1_fa")
    @Expose
    private String valrepLandimpImpsummary1Fa;
    @SerializedName("valrep_landimp_impsummary1_fa_per_td")
    @Expose
    private String valrepLandimpImpsummary1FaPerTd;
    @SerializedName("valrep_landimp_impsummary1_index")
    @Expose
    private String valrepLandimpImpsummary1Index;
    @SerializedName("valrep_landimp_impsummary1_la")
    @Expose
    private String valrepLandimpImpsummary1La;
    @SerializedName("valrep_landimp_impsummary1_no_of_bedrooms")
    @Expose
    private String valrepLandimpImpsummary1NoOfBedrooms;
    @SerializedName("valrep_landimp_impsummary1_no_of_floors")
    @Expose
    private String valrepLandimpImpsummary1NoOfFloors;
    @SerializedName("valrep_landimp_impsummary1_owner")
    @Expose
    private String valrepLandimpImpsummary1Owner;
    @SerializedName("valrep_landimp_impsummary1_ownership_of_property")
    @Expose
    private String valrepLandimpImpsummary1OwnershipOfProperty;
    @SerializedName("valrep_landimp_impsummary1_socialized_housing")
    @Expose
    private String valrepLandimpImpsummary1SocializedHousing;
    @SerializedName("valrep_landimp_impsummary1_standard_features")
    @Expose
    private String valrepLandimpImpsummary1StandardFeatures;
    @SerializedName("valrep_landimp_impsummary1_usage_declaration")
    @Expose
    private String valrepLandimpImpsummary1UsageDeclaration;
    @SerializedName("valrep_landimp_tax_dec_imp_no")
    @Expose
    private String valrepLandimpTaxDecImpNo;
    @SerializedName("valrep_landimp_tax_dec_imp_td_classification")
    @Expose
    private String valrepLandimpTaxDecImpTdClassification;
    @SerializedName("valrep_landimp_tax_dec_imp_td_declarant")
    @Expose
    private String valrepLandimpTaxDecImpTdDeclarant;

    public String getValrepLandimpDescBeams() {
        return valrepLandimpDescBeams;
    }

    public void setValrepLandimpDescBeams(String valrepLandimpDescBeams) {
        this.valrepLandimpDescBeams = valrepLandimpDescBeams;
    }

    public String getValrepLandimpDescCategoryClass() {
        return valrepLandimpDescCategoryClass;
    }

    public void setValrepLandimpDescCategoryClass(String valrepLandimpDescCategoryClass) {
        this.valrepLandimpDescCategoryClass = valrepLandimpDescCategoryClass;
    }

    public String getValrepLandimpDescCeiling() {
        return valrepLandimpDescCeiling;
    }

    public void setValrepLandimpDescCeiling(String valrepLandimpDescCeiling) {
        this.valrepLandimpDescCeiling = valrepLandimpDescCeiling;
    }

    public String getValrepLandimpDescColumnsPosts() {
        return valrepLandimpDescColumnsPosts;
    }

    public void setValrepLandimpDescColumnsPosts(String valrepLandimpDescColumnsPosts) {
        this.valrepLandimpDescColumnsPosts = valrepLandimpDescColumnsPosts;
    }

    public String getValrepLandimpDescCompartments() {
        return valrepLandimpDescCompartments;
    }

    public void setValrepLandimpDescCompartments(String valrepLandimpDescCompartments) {
        this.valrepLandimpDescCompartments = valrepLandimpDescCompartments;
    }

    public String getValrepLandimpDescConfirmedThru() {
        return valrepLandimpDescConfirmedThru;
    }

    public void setValrepLandimpDescConfirmedThru(String valrepLandimpDescConfirmedThru) {
        this.valrepLandimpDescConfirmedThru = valrepLandimpDescConfirmedThru;
    }

    public String getValrepLandimpDescConstructionFeature() {
        return valrepLandimpDescConstructionFeature;
    }

    public void setValrepLandimpDescConstructionFeature(String valrepLandimpDescConstructionFeature) {
        this.valrepLandimpDescConstructionFeature = valrepLandimpDescConstructionFeature;
    }

    public String getValrepLandimpDescDoors() {
        return valrepLandimpDescDoors;
    }

    public void setValrepLandimpDescDoors(String valrepLandimpDescDoors) {
        this.valrepLandimpDescDoors = valrepLandimpDescDoors;
    }

    public String getValrepLandimpDescEconomicLife() {
        return valrepLandimpDescEconomicLife;
    }

    public void setValrepLandimpDescEconomicLife(String valrepLandimpDescEconomicLife) {
        this.valrepLandimpDescEconomicLife = valrepLandimpDescEconomicLife;
    }

    public String getValrepLandimpDescEffectiveAge() {
        return valrepLandimpDescEffectiveAge;
    }

    public void setValrepLandimpDescEffectiveAge(String valrepLandimpDescEffectiveAge) {
        this.valrepLandimpDescEffectiveAge = valrepLandimpDescEffectiveAge;
    }

    public String getValrepLandimpDescEstLife() {
        return valrepLandimpDescEstLife;
    }

    public void setValrepLandimpDescEstLife(String valrepLandimpDescEstLife) {
        this.valrepLandimpDescEstLife = valrepLandimpDescEstLife;
    }

    public String getValrepLandimpDescExteriorWalls() {
        return valrepLandimpDescExteriorWalls;
    }

    public void setValrepLandimpDescExteriorWalls(String valrepLandimpDescExteriorWalls) {
        this.valrepLandimpDescExteriorWalls = valrepLandimpDescExteriorWalls;
    }

    public String getValrepLandimpDescFeatures2f() {
        return valrepLandimpDescFeatures2f;
    }

    public void setValrepLandimpDescFeatures2f(String valrepLandimpDescFeatures2f) {
        this.valrepLandimpDescFeatures2f = valrepLandimpDescFeatures2f;
    }

    public String getValrepLandimpDescFeatures3f() {
        return valrepLandimpDescFeatures3f;
    }

    public void setValrepLandimpDescFeatures3f(String valrepLandimpDescFeatures3f) {
        this.valrepLandimpDescFeatures3f = valrepLandimpDescFeatures3f;
    }

    public String getValrepLandimpDescFeatures4f() {
        return valrepLandimpDescFeatures4f;
    }

    public void setValrepLandimpDescFeatures4f(String valrepLandimpDescFeatures4f) {
        this.valrepLandimpDescFeatures4f = valrepLandimpDescFeatures4f;
    }

    public String getValrepLandimpDescFeaturesGf() {
        return valrepLandimpDescFeaturesGf;
    }

    public void setValrepLandimpDescFeaturesGf(String valrepLandimpDescFeaturesGf) {
        this.valrepLandimpDescFeaturesGf = valrepLandimpDescFeaturesGf;
    }

    public String getValrepLandimpDescFoundation() {
        return valrepLandimpDescFoundation;
    }

    public void setValrepLandimpDescFoundation(String valrepLandimpDescFoundation) {
        this.valrepLandimpDescFoundation = valrepLandimpDescFoundation;
    }

    public String getValrepLandimpDescImpCostGrade() {
        return valrepLandimpDescImpCostGrade;
    }

    public void setValrepLandimpDescImpCostGrade(String valrepLandimpDescImpCostGrade) {
        this.valrepLandimpDescImpCostGrade = valrepLandimpDescImpCostGrade;
    }

    public String getValrepLandimpDescImpDesc() {
        return valrepLandimpDescImpDesc;
    }

    public void setValrepLandimpDescImpDesc(String valrepLandimpDescImpDesc) {
        this.valrepLandimpDescImpDesc = valrepLandimpDescImpDesc;
    }

    public String getValrepLandimpDescImpFence() {
        return valrepLandimpDescImpFence;
    }

    public void setValrepLandimpDescImpFence(String valrepLandimpDescImpFence) {
        this.valrepLandimpDescImpFence = valrepLandimpDescImpFence;
    }

    public String getValrepLandimpDescImpFloorArea() {
        return valrepLandimpDescImpFloorArea;
    }

    public void setValrepLandimpDescImpFloorArea(String valrepLandimpDescImpFloorArea) {
        this.valrepLandimpDescImpFloorArea = valrepLandimpDescImpFloorArea;
    }

    public String getValrepLandimpDescImpFlooring() {
        return valrepLandimpDescImpFlooring;
    }

    public void setValrepLandimpDescImpFlooring(String valrepLandimpDescImpFlooring) {
        this.valrepLandimpDescImpFlooring = valrepLandimpDescImpFlooring;
    }

    public String getValrepLandimpDescImpFraming() {
        return valrepLandimpDescImpFraming;
    }

    public void setValrepLandimpDescImpFraming(String valrepLandimpDescImpFraming) {
        this.valrepLandimpDescImpFraming = valrepLandimpDescImpFraming;
    }

    public String getValrepLandimpDescImpGarage() {
        return valrepLandimpDescImpGarage;
    }

    public void setValrepLandimpDescImpGarage(String valrepLandimpDescImpGarage) {
        this.valrepLandimpDescImpGarage = valrepLandimpDescImpGarage;
    }

    public String getValrepLandimpDescImpNoStorey() {
        return valrepLandimpDescImpNoStorey;
    }

    public void setValrepLandimpDescImpNoStorey(String valrepLandimpDescImpNoStorey) {
        this.valrepLandimpDescImpNoStorey = valrepLandimpDescImpNoStorey;
    }

    public String getValrepLandimpDescImpOthers() {
        return valrepLandimpDescImpOthers;
    }

    public void setValrepLandimpDescImpOthers(String valrepLandimpDescImpOthers) {
        this.valrepLandimpDescImpOthers = valrepLandimpDescImpOthers;
    }

    public String getValrepLandimpDescImpRemainLife() {
        return valrepLandimpDescImpRemainLife;
    }

    public void setValrepLandimpDescImpRemainLife(String valrepLandimpDescImpRemainLife) {
        this.valrepLandimpDescImpRemainLife = valrepLandimpDescImpRemainLife;
    }

    public String getValrepLandimpDescImpRoofing() {
        return valrepLandimpDescImpRoofing;
    }

    public void setValrepLandimpDescImpRoofing(String valrepLandimpDescImpRoofing) {
        this.valrepLandimpDescImpRoofing = valrepLandimpDescImpRoofing;
    }

    public String getValrepLandimpDescImpRooms() {
        return valrepLandimpDescImpRooms;
    }

    public void setValrepLandimpDescImpRooms(String valrepLandimpDescImpRooms) {
        this.valrepLandimpDescImpRooms = valrepLandimpDescImpRooms;
    }

    public String getValrepLandimpDescImpTAndB() {
        return valrepLandimpDescImpTAndB;
    }

    public void setValrepLandimpDescImpTAndB(String valrepLandimpDescImpTAndB) {
        this.valrepLandimpDescImpTAndB = valrepLandimpDescImpTAndB;
    }

    public String getValrepLandimpDescImpUse() {
        return valrepLandimpDescImpUse;
    }

    public void setValrepLandimpDescImpUse(String valrepLandimpDescImpUse) {
        this.valrepLandimpDescImpUse = valrepLandimpDescImpUse;
    }

    public String getValrepLandimpDescImpWalls() {
        return valrepLandimpDescImpWalls;
    }

    public void setValrepLandimpDescImpWalls(String valrepLandimpDescImpWalls) {
        this.valrepLandimpDescImpWalls = valrepLandimpDescImpWalls;
    }

    public String getValrepLandimpDescImpWindows() {
        return valrepLandimpDescImpWindows;
    }

    public void setValrepLandimpDescImpWindows(String valrepLandimpDescImpWindows) {
        this.valrepLandimpDescImpWindows = valrepLandimpDescImpWindows;
    }

    public String getValrepLandimpDescInteriorWalls() {
        return valrepLandimpDescInteriorWalls;
    }

    public void setValrepLandimpDescInteriorWalls(String valrepLandimpDescInteriorWalls) {
        this.valrepLandimpDescInteriorWalls = valrepLandimpDescInteriorWalls;
    }

    public String getValrepLandimpDescKitchen() {
        return valrepLandimpDescKitchen;
    }

    public void setValrepLandimpDescKitchen(String valrepLandimpDescKitchen) {
        this.valrepLandimpDescKitchen = valrepLandimpDescKitchen;
    }

    public String getValrepLandimpDescObservedCondition() {
        return valrepLandimpDescObservedCondition;
    }

    public void setValrepLandimpDescObservedCondition(String valrepLandimpDescObservedCondition) {
        this.valrepLandimpDescObservedCondition = valrepLandimpDescObservedCondition;
    }

    public String getValrepLandimpDescOccupants() {
        return valrepLandimpDescOccupants;
    }

    public void setValrepLandimpDescOccupants(String valrepLandimpDescOccupants) {
        this.valrepLandimpDescOccupants = valrepLandimpDescOccupants;
    }

    public String getValrepLandimpDescOwnedOrLeased() {
        return valrepLandimpDescOwnedOrLeased;
    }

    public void setValrepLandimpDescOwnedOrLeased(String valrepLandimpDescOwnedOrLeased) {
        this.valrepLandimpDescOwnedOrLeased = valrepLandimpDescOwnedOrLeased;
    }

    public String getValrepLandimpDescPartition() {
        return valrepLandimpDescPartition;
    }

    public void setValrepLandimpDescPartition(String valrepLandimpDescPartition) {
        this.valrepLandimpDescPartition = valrepLandimpDescPartition;
    }

    public String getValrepLandimpDescPartitions() {
        return valrepLandimpDescPartitions;
    }

    public void setValrepLandimpDescPartitions(String valrepLandimpDescPartitions) {
        this.valrepLandimpDescPartitions = valrepLandimpDescPartitions;
    }

    public String getValrepLandimpDescPercentCompleted() {
        return valrepLandimpDescPercentCompleted;
    }

    public void setValrepLandimpDescPercentCompleted(String valrepLandimpDescPercentCompleted) {
        this.valrepLandimpDescPercentCompleted = valrepLandimpDescPercentCompleted;
    }

    public String getValrepLandimpDescPropertyType() {
        return valrepLandimpDescPropertyType;
    }

    public void setValrepLandimpDescPropertyType(String valrepLandimpDescPropertyType) {
        this.valrepLandimpDescPropertyType = valrepLandimpDescPropertyType;
    }

    public String getValrepLandimpDescRoof() {
        return valrepLandimpDescRoof;
    }

    public void setValrepLandimpDescRoof(String valrepLandimpDescRoof) {
        this.valrepLandimpDescRoof = valrepLandimpDescRoof;
    }

    public String getValrepLandimpDescStairs() {
        return valrepLandimpDescStairs;
    }

    public void setValrepLandimpDescStairs(String valrepLandimpDescStairs) {
        this.valrepLandimpDescStairs = valrepLandimpDescStairs;
    }

    public String getValrepLandimpDescTb() {
        return valrepLandimpDescTb;
    }

    public void setValrepLandimpDescTb(String valrepLandimpDescTb) {
        this.valrepLandimpDescTb = valrepLandimpDescTb;
    }

    public String getValrepLandimpDescTrusses() {
        return valrepLandimpDescTrusses;
    }

    public void setValrepLandimpDescTrusses(String valrepLandimpDescTrusses) {
        this.valrepLandimpDescTrusses = valrepLandimpDescTrusses;
    }

    public String getValrepLandimpDescYearBuilt() {
        return valrepLandimpDescYearBuilt;
    }

    public void setValrepLandimpDescYearBuilt(String valrepLandimpDescYearBuilt) {
        this.valrepLandimpDescYearBuilt = valrepLandimpDescYearBuilt;
    }

    public String getValrepLandimpImpsummary1ActualUtilization() {
        return valrepLandimpImpsummary1ActualUtilization;
    }

    public void setValrepLandimpImpsummary1ActualUtilization(String valrepLandimpImpsummary1ActualUtilization) {
        this.valrepLandimpImpsummary1ActualUtilization = valrepLandimpImpsummary1ActualUtilization;
    }

    public String getValrepLandimpImpsummary1BuildingDesc() {
        return valrepLandimpImpsummary1BuildingDesc;
    }

    public void setValrepLandimpImpsummary1BuildingDesc(String valrepLandimpImpsummary1BuildingDesc) {
        this.valrepLandimpImpsummary1BuildingDesc = valrepLandimpImpsummary1BuildingDesc;
    }

    public String getValrepLandimpImpsummary1ErectedOnLot() {
        return valrepLandimpImpsummary1ErectedOnLot;
    }

    public void setValrepLandimpImpsummary1ErectedOnLot(String valrepLandimpImpsummary1ErectedOnLot) {
        this.valrepLandimpImpsummary1ErectedOnLot = valrepLandimpImpsummary1ErectedOnLot;
    }

    public String getValrepLandimpImpsummary1Fa() {
        return valrepLandimpImpsummary1Fa;
    }

    public void setValrepLandimpImpsummary1Fa(String valrepLandimpImpsummary1Fa) {
        this.valrepLandimpImpsummary1Fa = valrepLandimpImpsummary1Fa;
    }

    public String getValrepLandimpImpsummary1FaPerTd() {
        return valrepLandimpImpsummary1FaPerTd;
    }

    public void setValrepLandimpImpsummary1FaPerTd(String valrepLandimpImpsummary1FaPerTd) {
        this.valrepLandimpImpsummary1FaPerTd = valrepLandimpImpsummary1FaPerTd;
    }

    public String getValrepLandimpImpsummary1Index() {
        return valrepLandimpImpsummary1Index;
    }

    public void setValrepLandimpImpsummary1Index(String valrepLandimpImpsummary1Index) {
        this.valrepLandimpImpsummary1Index = valrepLandimpImpsummary1Index;
    }

    public String getValrepLandimpImpsummary1La() {
        return valrepLandimpImpsummary1La;
    }

    public void setValrepLandimpImpsummary1La(String valrepLandimpImpsummary1La) {
        this.valrepLandimpImpsummary1La = valrepLandimpImpsummary1La;
    }

    public String getValrepLandimpImpsummary1NoOfBedrooms() {
        return valrepLandimpImpsummary1NoOfBedrooms;
    }

    public void setValrepLandimpImpsummary1NoOfBedrooms(String valrepLandimpImpsummary1NoOfBedrooms) {
        this.valrepLandimpImpsummary1NoOfBedrooms = valrepLandimpImpsummary1NoOfBedrooms;
    }

    public String getValrepLandimpImpsummary1NoOfFloors() {
        return valrepLandimpImpsummary1NoOfFloors;
    }

    public void setValrepLandimpImpsummary1NoOfFloors(String valrepLandimpImpsummary1NoOfFloors) {
        this.valrepLandimpImpsummary1NoOfFloors = valrepLandimpImpsummary1NoOfFloors;
    }

    public String getValrepLandimpImpsummary1Owner() {
        return valrepLandimpImpsummary1Owner;
    }

    public void setValrepLandimpImpsummary1Owner(String valrepLandimpImpsummary1Owner) {
        this.valrepLandimpImpsummary1Owner = valrepLandimpImpsummary1Owner;
    }

    public String getValrepLandimpImpsummary1OwnershipOfProperty() {
        return valrepLandimpImpsummary1OwnershipOfProperty;
    }

    public void setValrepLandimpImpsummary1OwnershipOfProperty(String valrepLandimpImpsummary1OwnershipOfProperty) {
        this.valrepLandimpImpsummary1OwnershipOfProperty = valrepLandimpImpsummary1OwnershipOfProperty;
    }

    public String getValrepLandimpImpsummary1SocializedHousing() {
        return valrepLandimpImpsummary1SocializedHousing;
    }

    public void setValrepLandimpImpsummary1SocializedHousing(String valrepLandimpImpsummary1SocializedHousing) {
        this.valrepLandimpImpsummary1SocializedHousing = valrepLandimpImpsummary1SocializedHousing;
    }

    public String getValrepLandimpImpsummary1StandardFeatures() {
        return valrepLandimpImpsummary1StandardFeatures;
    }

    public void setValrepLandimpImpsummary1StandardFeatures(String valrepLandimpImpsummary1StandardFeatures) {
        this.valrepLandimpImpsummary1StandardFeatures = valrepLandimpImpsummary1StandardFeatures;
    }

    public String getValrepLandimpImpsummary1UsageDeclaration() {
        return valrepLandimpImpsummary1UsageDeclaration;
    }

    public void setValrepLandimpImpsummary1UsageDeclaration(String valrepLandimpImpsummary1UsageDeclaration) {
        this.valrepLandimpImpsummary1UsageDeclaration = valrepLandimpImpsummary1UsageDeclaration;
    }

    public String getValrepLandimpTaxDecImpNo() {
        return valrepLandimpTaxDecImpNo;
    }

    public void setValrepLandimpTaxDecImpNo(String valrepLandimpTaxDecImpNo) {
        this.valrepLandimpTaxDecImpNo = valrepLandimpTaxDecImpNo;
    }

    public String getValrepLandimpTaxDecImpTdClassification() {
        return valrepLandimpTaxDecImpTdClassification;
    }

    public void setValrepLandimpTaxDecImpTdClassification(String valrepLandimpTaxDecImpTdClassification) {
        this.valrepLandimpTaxDecImpTdClassification = valrepLandimpTaxDecImpTdClassification;
    }

    public String getValrepLandimpTaxDecImpTdDeclarant() {
        return valrepLandimpTaxDecImpTdDeclarant;
    }

    public void setValrepLandimpTaxDecImpTdDeclarant(String valrepLandimpTaxDecImpTdDeclarant) {
        this.valrepLandimpTaxDecImpTdDeclarant = valrepLandimpTaxDecImpTdDeclarant;
    }

}