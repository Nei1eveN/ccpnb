package appraisal.com.gds.ccpnb.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppCollateralAddress {

    @SerializedName("app_bldg")
    @Expose
    private String appBldg;
    @SerializedName("app_block_no")
    @Expose
    private String appBlockNo;
    @SerializedName("app_city")
    @Expose
    private String appCity;
    @SerializedName("app_country")
    @Expose
    private String appCountry;
    @SerializedName("app_district")
    @Expose
    private String appDistrict;
    @SerializedName("app_lot_no")
    @Expose
    private String appLotNo;
    @SerializedName("app_province")
    @Expose
    private String appProvince;
    @SerializedName("app_region")
    @Expose
    private String appRegion;
    @SerializedName("app_street_name")
    @Expose
    private String appStreetName;
    @SerializedName("app_street_no")
    @Expose
    private Integer appStreetNo;
    @SerializedName("app_tct_no")
    @Expose
    private Integer appTctNo;
    @SerializedName("app_unit_no")
    @Expose
    private String appUnitNo;
    @SerializedName("app_village")
    @Expose
    private String appVillage;
    @SerializedName("app_zip")
    @Expose
    private Integer appZip;

    public String getAppBldg() {
        return appBldg;
    }

    public void setAppBldg(String appBldg) {
        this.appBldg = appBldg;
    }

    public String getAppBlockNo() {
        return appBlockNo;
    }

    public void setAppBlockNo(String appBlockNo) {
        this.appBlockNo = appBlockNo;
    }

    public String getAppCity() {
        return appCity;
    }

    public void setAppCity(String appCity) {
        this.appCity = appCity;
    }

    public String getAppCountry() {
        return appCountry;
    }

    public void setAppCountry(String appCountry) {
        this.appCountry = appCountry;
    }

    public String getAppDistrict() {
        return appDistrict;
    }

    public void setAppDistrict(String appDistrict) {
        this.appDistrict = appDistrict;
    }

    public String getAppLotNo() {
        return appLotNo;
    }

    public void setAppLotNo(String appLotNo) {
        this.appLotNo = appLotNo;
    }

    public String getAppProvince() {
        return appProvince;
    }

    public void setAppProvince(String appProvince) {
        this.appProvince = appProvince;
    }

    public String getAppRegion() {
        return appRegion;
    }

    public void setAppRegion(String appRegion) {
        this.appRegion = appRegion;
    }

    public String getAppStreetName() {
        return appStreetName;
    }

    public void setAppStreetName(String appStreetName) {
        this.appStreetName = appStreetName;
    }

    public Integer getAppStreetNo() {
        return appStreetNo;
    }

    public void setAppStreetNo(Integer appStreetNo) {
        this.appStreetNo = appStreetNo;
    }

    public Integer getAppTctNo() {
        return appTctNo;
    }

    public void setAppTctNo(Integer appTctNo) {
        this.appTctNo = appTctNo;
    }

    public String getAppUnitNo() {
        return appUnitNo;
    }

    public void setAppUnitNo(String appUnitNo) {
        this.appUnitNo = appUnitNo;
    }

    public String getAppVillage() {
        return appVillage;
    }

    public void setAppVillage(String appVillage) {
        this.appVillage = appVillage;
    }

    public Integer getAppZip() {
        return appZip;
    }

    public void setAppZip(Integer appZip) {
        this.appZip = appZip;
    }

}