package appraisal.com.gds.ccpnb.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Constants {
    // Shared preferences file name
    static final String PREF_NAME = "UserLogin";

    static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    public static String KEY_USER_EMAIL = "email";
    public static String KEY_USER_ROLE = "user_role";

    //USER ROLES
    public static final String USER_APPRAISER = "Appraiser";

    //URL SIGN IN
    public static String PNB_URL_SIGN_IN = "http://dev2.gdslinkasia.com/users/sign_in.json";

    //URL RECORDS
    public static String PNB_URL_RECORDS = "http://dev2.gdslinkasia.com/philippine_national_bank/appraisal/records.json";

    //URL VIEW RECORD
    public static String PNB_URL_VIEW_RECORD = "http://dev2.gdslinkasia.com/philippine_national_bank/appraisal/records/show.json";

    //URL UPDATE RECORD
    public static String PNB_URL_UPDATE_RECORD = "http://dev2.gdslinkasia.com/philippine_national_bank/appraisal/records/update.json";

    //SIGN IN PARAMETERS
    public static String EMAIL_PARAM = "user[email]";
    public static String PASSWORD_PARAM = "user[password]";

    //AUTH_TOKEN PARAMETERS
    public static String USER_AUTH_TOKEN = "auth_token";
    public static String AUTH_TOKEN_VALUE = "yfFTAW7dapdiZwxaqBus";

    //QUERY PARAMETERS
    public static String QUERY_PARAM = "query";
    public static String APPLICATION_SIMUL_TYPE = "app_simul_type";
    public static String SIMUL_TYPE = "sub";
    public static String APPLICATION_STATUS_HEADER = "application_status";
    public static String APPR_REQUEST_USER_NAME = "appraisal_request.appraiser_username";
    public static String SYSTEM_HIDDEN = "system.hidden";

    //LIMIT PARAMETERS
    public static String LIMIT_PARAM = "limit";

    //ONLY PARAMETERS
    public static String ONLY_PARAM = "only";
    public static String SYSTEM_RECORD_ID = "system.record_id";
    public static String ACCOUNT_FIRST_NAME = "app_account_first_name";
    public static String ACCOUNT_MIDDLE_NAME = "app_account_middle_name";
    public static String ACCOUNT_LAST_NAME = "app_account_last_name";
    private static String ONLY_PARAM_APP_REQUEST_APPRAISAL = "appraisal_request.app_request_appraisal";
    public static String ONLY_PARAM_VALUE = "{\n" + "  \""+SYSTEM_RECORD_ID+"\": {},\n" + "  \""+ACCOUNT_FIRST_NAME+"\": {},\n" + "  \""+ACCOUNT_MIDDLE_NAME+"\": {},\n" + "  \""+ACCOUNT_LAST_NAME+"\": {},\n" + "  \""+ONLY_PARAM_APP_REQUEST_APPRAISAL+"\": {}\n" + "}";

    //DATA PARAMETERS
    public static String DATA_PARAM = "data";


    //PENDING / FOR JOB ACCEPTANCE VALUES
    public static String STATUS_JOB_ACCEPTANCE = "appraiser_job_acceptance";

    public static String STATUS_JOB_ACCEPTED = "accepted_job";

    //REWORK VALUES
    public static String STATUS_FOR_REWORK = "for_rework";
    public static String REWORK_ACCEPTED_HEADER = "rework_accepted";

    //OBJECT SERIALIZED NAMES
    public static String COMPANY_SERIAL_NAME = "company";
    public static String USER_SERIAL_NAME = "user";

    public static String RECORD = "record";
    public static String RECORDS = "records";
    public static String APPRAISAL_REQUEST = "appraisal_request";
    public static String SYSTEM = "system";

    public static String RECORD_ID = "record_id";
    public static String APP_REQ_APPRAISAL = "app_request_appraisal";
    public static String APP_PRIORITY = "app_priority";

    //Check Network Availability
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
