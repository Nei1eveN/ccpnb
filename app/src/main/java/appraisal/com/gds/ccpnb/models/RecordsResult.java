package appraisal.com.gds.ccpnb.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecordsResult { //extends RealmObject
    @SerializedName("appraisal_request")
    @Expose
    private List<AppraisalRequest> appraisalRequest;

    @SerializedName("system")
    @Expose
    private System system;

    @SerializedName("jobAcceptanceRecords")
    @Expose
    private List<JobAcceptanceRecord> jobAcceptanceRecords;

    public List<JobAcceptanceRecord> getJobAcceptanceRecords() {
        return jobAcceptanceRecords;
    }

    public void setJobAcceptanceRecords(List<JobAcceptanceRecord> jobAcceptanceRecords) {
        this.jobAcceptanceRecords = jobAcceptanceRecords;
    }

    public List<AppraisalRequest> getAppraisalRequest() {
        return appraisalRequest;
    }

    public void setAppraisalRequest(List<AppraisalRequest> appraisalRequest) {
        this.appraisalRequest = appraisalRequest;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }
}
