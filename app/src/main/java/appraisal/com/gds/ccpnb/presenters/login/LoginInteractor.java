package appraisal.com.gds.ccpnb.presenters.login;

import android.content.Intent;

public interface LoginInteractor {
    interface LoginListener {
        void onLoginSuccess(String title, String message, Intent userData);

        void onLoginFailure(String errorTitle, String errorMessage);
    }

    void getLoginCredentials(String email, String password, String userType, LoginListener listener);
}
