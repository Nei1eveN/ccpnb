package appraisal.com.gds.ccpnb.presenters.splash;

import android.content.Intent;

public interface SplashPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setCaptionText(String message);
        void goToDesignatedPage(Intent resultData);
    }
    void onStart();
    void onDestroy();
}
