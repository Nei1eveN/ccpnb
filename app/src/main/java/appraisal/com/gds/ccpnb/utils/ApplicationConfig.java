package appraisal.com.gds.ccpnb.utils;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class ApplicationConfig extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidNetworking.initialize(this);

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(config);
    }
}
