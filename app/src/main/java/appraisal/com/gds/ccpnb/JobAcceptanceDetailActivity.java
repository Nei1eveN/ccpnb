package appraisal.com.gds.ccpnb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import appraisal.com.gds.ccpnb.adapters.JobAcceptanceDetailAdapter;
import appraisal.com.gds.ccpnb.presenters.PendingDetailPresenter;
import appraisal.com.gds.ccpnb.presenters.PendingDetailPresenterImpl;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import static appraisal.com.gds.ccpnb.utils.Constants.RECORD_ID;

public class JobAcceptanceDetailActivity extends AppCompatActivity implements PendingDetailPresenter.View, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.coorList)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.swipeList)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.ivEmptyList)
    ImageView emptyImage;
    @BindView(R.id.tvEmptyList)
    TextView emptyText;
    @BindView(R.id.rvList)
    RecyclerView recyclerView;

    Intent intent;
    String record_id;

    ProgressDialog progressDialog;

    PendingDetailPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_acceptance_detail);
        ButterKnife.bind(this);

        intent = getIntent();
        record_id = intent.getStringExtra(RECORD_ID);

        progressDialog = new ProgressDialog(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        presenter = new PendingDetailPresenterImpl(this, this);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart(record_id);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        swipeRefreshLayout.setRefreshing(true);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
        progressDialog.hide();
    }

    @Override
    public void setListDetail(JobAcceptanceDetailAdapter adapter) {
        emptyImage.setVisibility(View.GONE);
        emptyText.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setEmptyState(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
        recyclerView.setVisibility(View.GONE);

        emptyImage.setVisibility(View.VISIBLE);
        emptyText.setVisibility(View.VISIBLE);

        emptyText.setText(message);
    }

    @Override
    public void onRefresh() {
        presenter.requestDetails(record_id);
    }
}
