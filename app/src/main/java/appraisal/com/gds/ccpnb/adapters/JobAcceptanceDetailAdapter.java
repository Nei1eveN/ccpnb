package appraisal.com.gds.ccpnb.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import appraisal.com.gds.ccpnb.R;
import appraisal.com.gds.ccpnb.models.PendingJobDetails;
import butterknife.BindView;
import butterknife.ButterKnife;

public class JobAcceptanceDetailAdapter extends RecyclerView.Adapter<JobAcceptanceDetailAdapter.AcceptanceDetailViewHolder> {
    private Context context;
    private List<PendingJobDetails> details;

    public JobAcceptanceDetailAdapter(Context context, List<PendingJobDetails> details) {
        this.context = context;
        this.details = details;
    }

    static class AcceptanceDetailViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.constraintLayout)
        ConstraintLayout constraintLayout;
        @BindView(R.id.tvDetailTitle)
        TextView title;
        @BindView(R.id.tvDetailDesc) TextView description;

        AcceptanceDetailViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public AcceptanceDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AcceptanceDetailViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_job_detail_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AcceptanceDetailViewHolder holder, int position) {
        PendingJobDetails detail = details.get(position);

        holder.title.setText(detail.getTitle());
        holder.description.setText(detail.getContent());
    }

    @Override
    public int getItemCount() {
        return details.size();
    }
}
