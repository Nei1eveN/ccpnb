package appraisal.com.gds.ccpnb.presenters.records.rework;

import android.content.Context;

import appraisal.com.gds.ccpnb.adapters.ReworkAdapter;

public class ReworkPresenterImpl implements ReworkPresenter, ReworkInteractor.RecordsListener {
    private ReworkPresenter.View view;
    private ReworkInteractor interactor;

    public ReworkPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new ReworkInteractorImpl(context);
    }

    @Override
    public void onStart(String email) {
        if (view != null) {
            view.showProgress("Loading Rework Appraisals", "Please wait...");
//            view.hideViews();
        }
        interactor.getOfflineData(email, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestRecords(String email) {
        if (view != null) {
            view.showProgress("Fetching Data", "Please wait...");
        }
        interactor.getRecords(email, this);
    }

    @Override
    public void onListSuccess(ReworkAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.setList(adapter);
        }
    }

    @Override
    public void onListFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
        }
    }
}
