package appraisal.com.gds.ccpnb.presenters.records.pending;

import android.content.Context;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.TimeUnit;

import appraisal.com.gds.ccpnb.adapters.JobAcceptanceAdapter;
import appraisal.com.gds.ccpnb.models.JobAcceptanceRecord;
import io.realm.Realm;
import io.realm.RealmResults;

import static appraisal.com.gds.ccpnb.utils.Constants.ACCOUNT_FIRST_NAME;
import static appraisal.com.gds.ccpnb.utils.Constants.ACCOUNT_LAST_NAME;
import static appraisal.com.gds.ccpnb.utils.Constants.ACCOUNT_MIDDLE_NAME;
import static appraisal.com.gds.ccpnb.utils.Constants.APPLICATION_SIMUL_TYPE;
import static appraisal.com.gds.ccpnb.utils.Constants.APPLICATION_STATUS_HEADER;
import static appraisal.com.gds.ccpnb.utils.Constants.STATUS_JOB_ACCEPTANCE;
import static appraisal.com.gds.ccpnb.utils.Constants.APPRAISAL_REQUEST;
import static appraisal.com.gds.ccpnb.utils.Constants.APPR_REQUEST_USER_NAME;
import static appraisal.com.gds.ccpnb.utils.Constants.APP_PRIORITY;
import static appraisal.com.gds.ccpnb.utils.Constants.APP_REQ_APPRAISAL;
import static appraisal.com.gds.ccpnb.utils.Constants.AUTH_TOKEN_VALUE;
import static appraisal.com.gds.ccpnb.utils.Constants.LIMIT_PARAM;
import static appraisal.com.gds.ccpnb.utils.Constants.ONLY_PARAM;
import static appraisal.com.gds.ccpnb.utils.Constants.ONLY_PARAM_VALUE;
import static appraisal.com.gds.ccpnb.utils.Constants.PNB_URL_RECORDS;
import static appraisal.com.gds.ccpnb.utils.Constants.QUERY_PARAM;
import static appraisal.com.gds.ccpnb.utils.Constants.RECORDS;
import static appraisal.com.gds.ccpnb.utils.Constants.RECORD_ID;
import static appraisal.com.gds.ccpnb.utils.Constants.SIMUL_TYPE;
import static appraisal.com.gds.ccpnb.utils.Constants.SYSTEM;
import static appraisal.com.gds.ccpnb.utils.Constants.SYSTEM_HIDDEN;
import static appraisal.com.gds.ccpnb.utils.Constants.USER_AUTH_TOKEN;
import static appraisal.com.gds.ccpnb.utils.Constants.isNetworkAvailable;

class JobAcceptanceInteractorImpl implements JobAcceptanceInteractor {
    private Context context;
    private Realm realm = Realm.getDefaultInstance();

    JobAcceptanceInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getRecords(String email, RecordsListener listener) {
        if (!isNetworkAvailable(context)) {
            getOfflineData(email, listener);
        } else {
            JSONObject queryObject = new JSONObject();
            try {
                queryObject.put(APPLICATION_SIMUL_TYPE, SIMUL_TYPE);
                queryObject.put(APPLICATION_STATUS_HEADER, STATUS_JOB_ACCEPTANCE);
                queryObject.put(APPR_REQUEST_USER_NAME, email);
                queryObject.put(SYSTEM_HIDDEN, false);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d("interactor--email", email);
            Log.d("interactor--JSON", String.valueOf(queryObject));

            AndroidNetworking.post(PNB_URL_RECORDS)
                    .addBodyParameter(USER_AUTH_TOKEN, AUTH_TOKEN_VALUE)
                    .addBodyParameter(QUERY_PARAM, String.valueOf(queryObject))
                    .addBodyParameter(LIMIT_PARAM, String.valueOf(0))
                    .addBodyParameter(ONLY_PARAM, ONLY_PARAM_VALUE)
                    .setPriority(Priority.MEDIUM)
                    .setMaxAgeCacheControl(5, TimeUnit.DAYS)
                    .setMaxStaleCacheControl(365, TimeUnit.SECONDS)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("interactor--Response", String.valueOf(response));
                            try {
                                /**RECORD ARRAY**/
                                JSONArray jsonArray = response.getJSONArray(RECORDS);
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JobAcceptanceRecord record = new JobAcceptanceRecord();

                                    /**RECORD OBJECT**/
                                    JSONObject recordObject = jsonArray.getJSONObject(i);
                                    Log.d("interactor--RecordLoop", String.valueOf(recordObject));

                                    record.setAppAccountFirstName(recordObject.getString(ACCOUNT_FIRST_NAME));
                                    record.setAppAccountMiddleName(recordObject.getString(ACCOUNT_MIDDLE_NAME));
                                    record.setAppAccountLastName(recordObject.getString(ACCOUNT_LAST_NAME));

                                    /**APPRAISAL REQUEST ARRAY**/
                                    JSONArray app_request_array = recordObject.getJSONArray(APPRAISAL_REQUEST);
                                    for (int j = 0; j < app_request_array.length(); j++) {
                                        /**APPRAISAL REQUEST OBJECT**/
                                        JSONObject appraisalRequestObject = app_request_array.getJSONObject(j);

                                        Log.d("interactor--AppReqLoop", String.valueOf(appraisalRequestObject));

                                        record.setAppRequestAppraisal(appraisalRequestObject.getString(APP_REQ_APPRAISAL));
                                        record.setAppPriority(appraisalRequestObject.getString(APP_PRIORITY));
                                    }

                                    /**SYSTEM OBJECT**/
                                    JSONObject systemObject = recordObject.getJSONObject(SYSTEM);
                                    Log.d("interactor--SystemObj", String.valueOf(systemObject));

                                    record.setRecord_id(systemObject.getString(RECORD_ID));

                                    record.setApplicationStatus(STATUS_JOB_ACCEPTANCE);

                                    realm.beginTransaction();
                                    realm.copyToRealmOrUpdate(record);
                                    realm.commitTransaction();

                                    if (i == (jsonArray.length() - 1)) {
                                        getOfflineData(email, listener);
                                    }

                                }
                            } catch (JSONException e) {
                                listener.onListFailure("JSON Error:\n"+e.getLocalizedMessage());
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            listener.onListFailure(anError.getMessage());
                        }
                    });
        }

    }

    @Override
    public void getOfflineData(String email, RecordsListener listener) {
        RealmResults<JobAcceptanceRecord> recordRealmResults = realm.where(JobAcceptanceRecord.class).equalTo("applicationStatus", STATUS_JOB_ACCEPTANCE).findAll();
        if (!recordRealmResults.isEmpty()) {
            List<JobAcceptanceRecord> jobAcceptanceRecords = realm.copyFromRealm(recordRealmResults);

            JobAcceptanceAdapter adapter = new JobAcceptanceAdapter(context, jobAcceptanceRecords);
            listener.onListSuccess(adapter);
        } else {
            if (isNetworkAvailable(context)) {
                getRecords(email, listener);
            } else {
                listener.onListFailure("No Data Available.\n\nTo Retrieve Data from server, swipe down the screen.\n\nPlease check your network connection.");
            }

        }
    }
}
