package appraisal.com.gds.ccpnb.presenters.login;

import android.content.Context;
import android.content.Intent;

public class LoginPresenterImpl implements LoginPresenter, LoginInteractor.LoginListener {
    private LoginPresenter.View view;
    private LoginInteractor interactor;

    public LoginPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new LoginInteractorImpl(context);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void submitLoginCredentials(String email, String password, String userType) {
        if (email.isEmpty()) {
            view.showErrorDialog("Empty Email", "You cannot submit an empty email");
        } else if (password.isEmpty()) {
            view.showErrorDialog("Empty Password", "You cannot put an empty password");
        } else {
            if (view != null) {
                view.showProgress("Submitting Credentials", "Please wait...");
            }
            interactor.getLoginCredentials(email, password, userType, this);
        }
    }

    @Override
    public void onLoginSuccess(String title, String message, Intent userData) {
        if (view != null) {
            view.hideProgress();
            view.showUserPageDialog(title, message, userData);
        }
    }

    @Override
    public void onLoginFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }
}
