package appraisal.com.gds.ccpnb.presenters.records.rework;

import appraisal.com.gds.ccpnb.adapters.ReworkAdapter;

public interface ReworkPresenter {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void setList(ReworkAdapter adapter);
        void setEmptyState(String message);
    }
    void onStart(String email);
    void onDestroy();
    void requestRecords(String email);
}
