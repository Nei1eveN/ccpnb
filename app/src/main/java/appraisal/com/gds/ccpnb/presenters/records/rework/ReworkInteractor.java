package appraisal.com.gds.ccpnb.presenters.records.rework;

import appraisal.com.gds.ccpnb.adapters.ReworkAdapter;

public interface ReworkInteractor {
    interface RecordsListener {
        void onListSuccess(ReworkAdapter adapter);
        void onListFailure(String errorMessage);
    }
    void getRecords(String email, RecordsListener listener);
    void getOfflineData(String email, RecordsListener listener);
}
