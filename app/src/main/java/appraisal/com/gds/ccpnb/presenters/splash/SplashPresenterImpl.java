package appraisal.com.gds.ccpnb.presenters.splash;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

public class SplashPresenterImpl implements SplashPresenter, SplashInteractor.FindUserListener {
    private SplashPresenter.View view;
    private SplashInteractor interactor;

    public SplashPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new SplashInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.showProgress();
            view.setCaptionText("Checking if there is a Current User...");
        }
        new Handler().postDelayed(() -> interactor.getUser(this), 2000);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void onUserFound(String message, Intent resultData) {
        if (view != null) {
            view.hideProgress();
            view.setCaptionText(message);
            view.goToDesignatedPage(resultData);
        }
    }

    @Override
    public void onUserNotFound(String message, Intent resultData) {
        if (view != null) {
            view.hideProgress();
            view.setCaptionText(message);
            view.goToDesignatedPage(resultData);
        }
    }
}
