package appraisal.com.gds.ccpnb.presenters.records.pending;

import appraisal.com.gds.ccpnb.adapters.JobAcceptanceAdapter;

public interface JobAcceptancePresenter {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void setList(JobAcceptanceAdapter adapter);
        void setEmptyState(String message);
    }
    void onStart(String email);
    void onDestroy();
    void requestRecords(String email);
}
