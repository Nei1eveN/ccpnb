package appraisal.com.gds.ccpnb.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class AppraisalRequest extends RealmObject {

    @SerializedName("app_priority")
    @Expose
    private String appPriority;
    @SerializedName("app_request_appraisal")
    @Expose
    private String appRequestAppraisal;

    public String getAppPriority() {
        return appPriority;
    }

    public void setAppPriority(String appPriority) {
        this.appPriority = appPriority;
    }

    public String getAppRequestAppraisal() {
        return appRequestAppraisal;
    }

    public void setAppRequestAppraisal(String appRequestAppraisal) {
        this.appRequestAppraisal = appRequestAppraisal;
    }

}
