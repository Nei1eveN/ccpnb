package appraisal.com.gds.ccpnb.presenters;

import appraisal.com.gds.ccpnb.adapters.JobAcceptanceDetailAdapter;

public interface PendingDetailPresenter {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void setListDetail(JobAcceptanceDetailAdapter adapter);
        void setEmptyState(String message);
    }
    void onStart(String record_id);
    void onDestroy();
    void requestDetails(String record_id);
}
