package appraisal.com.gds.ccpnb.presenters.records.pending;

import appraisal.com.gds.ccpnb.adapters.JobAcceptanceAdapter;

public interface JobAcceptanceInteractor {
    interface RecordsListener {
        void onListSuccess(JobAcceptanceAdapter adapter);
        void onListFailure(String errorMessage);
    }
    void getRecords(String email, RecordsListener listener);
    void getOfflineData(String email, RecordsListener listener);
}
