package appraisal.com.gds.ccpnb.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PendingJobDetails extends RealmObject {

//    @PrimaryKey
    private String record_id;

    private String content;
    private String title;

    public PendingJobDetails() {
    }

    public PendingJobDetails(String record_id, String content, String title) {
        this.record_id = record_id;
        this.content = content;
        this.title = title;
    }

    public String getRecord_id() {
        return record_id;
    }

    public void setRecord_id(String record_id) {
        this.record_id = record_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
