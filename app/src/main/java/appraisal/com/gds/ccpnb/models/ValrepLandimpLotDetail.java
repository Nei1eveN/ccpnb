package appraisal.com.gds.ccpnb.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValrepLandimpLotDetail {

    @SerializedName("valrep_landimp_propdesc_area")
    @Expose
    private String valrepLandimpPropdescArea;
    @SerializedName("valrep_landimp_propdesc_block")
    @Expose
    private String valrepLandimpPropdescBlock;
    @SerializedName("valrep_landimp_propdesc_deductions")
    @Expose
    private String valrepLandimpPropdescDeductions;
    @SerializedName("valrep_landimp_propdesc_index")
    @Expose
    private String valrepLandimpPropdescIndex;
    @SerializedName("valrep_landimp_propdesc_lot")
    @Expose
    private String valrepLandimpPropdescLot;
    @SerializedName("valrep_landimp_propdesc_place")
    @Expose
    private String valrepLandimpPropdescPlace;
    @SerializedName("valrep_landimp_propdesc_registered_owner")
    @Expose
    private String valrepLandimpPropdescRegisteredOwner;
    @SerializedName("valrep_landimp_propdesc_registry_date_day")
    @Expose
    private String valrepLandimpPropdescRegistryDateDay;
    @SerializedName("valrep_landimp_propdesc_registry_date_month")
    @Expose
    private String valrepLandimpPropdescRegistryDateMonth;
    @SerializedName("valrep_landimp_propdesc_registry_date_year")
    @Expose
    private String valrepLandimpPropdescRegistryDateYear;
    @SerializedName("valrep_landimp_propdesc_registry_of_deeds")
    @Expose
    private String valrepLandimpPropdescRegistryOfDeeds;
    @SerializedName("valrep_landimp_propdesc_survey_nos")
    @Expose
    private String valrepLandimpPropdescSurveyNos;
    @SerializedName("valrep_landimp_propdesc_tct_no")
    @Expose
    private String valrepLandimpPropdescTctNo;
    @SerializedName("valrep_landimp_propdesc_title_type")
    @Expose
    private String valrepLandimpPropdescTitleType;
    @SerializedName("valrep_landimp_tax_dec_classification")
    @Expose
    private String valrepLandimpTaxDecClassification;
    @SerializedName("valrep_landimp_tax_dec_declarant")
    @Expose
    private String valrepLandimpTaxDecDeclarant;
    @SerializedName("valrep_landimp_tax_dec_imp")
    @Expose
    private String valrepLandimpTaxDecImp;
    @SerializedName("valrep_landimp_tax_dec_lot")
    @Expose
    private String valrepLandimpTaxDecLot;
    @SerializedName("valrep_landimp_tax_dec_lot_no")
    @Expose
    private String valrepLandimpTaxDecLotNo;
    @SerializedName("valrep_table_app_legal_description")
    @Expose
    private String valrepTableAppLegalDescription;

    public String getValrepLandimpPropdescArea() {
        return valrepLandimpPropdescArea;
    }

    public void setValrepLandimpPropdescArea(String valrepLandimpPropdescArea) {
        this.valrepLandimpPropdescArea = valrepLandimpPropdescArea;
    }

    public String getValrepLandimpPropdescBlock() {
        return valrepLandimpPropdescBlock;
    }

    public void setValrepLandimpPropdescBlock(String valrepLandimpPropdescBlock) {
        this.valrepLandimpPropdescBlock = valrepLandimpPropdescBlock;
    }

    public String getValrepLandimpPropdescDeductions() {
        return valrepLandimpPropdescDeductions;
    }

    public void setValrepLandimpPropdescDeductions(String valrepLandimpPropdescDeductions) {
        this.valrepLandimpPropdescDeductions = valrepLandimpPropdescDeductions;
    }

    public String getValrepLandimpPropdescIndex() {
        return valrepLandimpPropdescIndex;
    }

    public void setValrepLandimpPropdescIndex(String valrepLandimpPropdescIndex) {
        this.valrepLandimpPropdescIndex = valrepLandimpPropdescIndex;
    }

    public String getValrepLandimpPropdescLot() {
        return valrepLandimpPropdescLot;
    }

    public void setValrepLandimpPropdescLot(String valrepLandimpPropdescLot) {
        this.valrepLandimpPropdescLot = valrepLandimpPropdescLot;
    }

    public String getValrepLandimpPropdescPlace() {
        return valrepLandimpPropdescPlace;
    }

    public void setValrepLandimpPropdescPlace(String valrepLandimpPropdescPlace) {
        this.valrepLandimpPropdescPlace = valrepLandimpPropdescPlace;
    }

    public String getValrepLandimpPropdescRegisteredOwner() {
        return valrepLandimpPropdescRegisteredOwner;
    }

    public void setValrepLandimpPropdescRegisteredOwner(String valrepLandimpPropdescRegisteredOwner) {
        this.valrepLandimpPropdescRegisteredOwner = valrepLandimpPropdescRegisteredOwner;
    }

    public String getValrepLandimpPropdescRegistryDateDay() {
        return valrepLandimpPropdescRegistryDateDay;
    }

    public void setValrepLandimpPropdescRegistryDateDay(String valrepLandimpPropdescRegistryDateDay) {
        this.valrepLandimpPropdescRegistryDateDay = valrepLandimpPropdescRegistryDateDay;
    }

    public String getValrepLandimpPropdescRegistryDateMonth() {
        return valrepLandimpPropdescRegistryDateMonth;
    }

    public void setValrepLandimpPropdescRegistryDateMonth(String valrepLandimpPropdescRegistryDateMonth) {
        this.valrepLandimpPropdescRegistryDateMonth = valrepLandimpPropdescRegistryDateMonth;
    }

    public String getValrepLandimpPropdescRegistryDateYear() {
        return valrepLandimpPropdescRegistryDateYear;
    }

    public void setValrepLandimpPropdescRegistryDateYear(String valrepLandimpPropdescRegistryDateYear) {
        this.valrepLandimpPropdescRegistryDateYear = valrepLandimpPropdescRegistryDateYear;
    }

    public String getValrepLandimpPropdescRegistryOfDeeds() {
        return valrepLandimpPropdescRegistryOfDeeds;
    }

    public void setValrepLandimpPropdescRegistryOfDeeds(String valrepLandimpPropdescRegistryOfDeeds) {
        this.valrepLandimpPropdescRegistryOfDeeds = valrepLandimpPropdescRegistryOfDeeds;
    }

    public String getValrepLandimpPropdescSurveyNos() {
        return valrepLandimpPropdescSurveyNos;
    }

    public void setValrepLandimpPropdescSurveyNos(String valrepLandimpPropdescSurveyNos) {
        this.valrepLandimpPropdescSurveyNos = valrepLandimpPropdescSurveyNos;
    }

    public String getValrepLandimpPropdescTctNo() {
        return valrepLandimpPropdescTctNo;
    }

    public void setValrepLandimpPropdescTctNo(String valrepLandimpPropdescTctNo) {
        this.valrepLandimpPropdescTctNo = valrepLandimpPropdescTctNo;
    }

    public String getValrepLandimpPropdescTitleType() {
        return valrepLandimpPropdescTitleType;
    }

    public void setValrepLandimpPropdescTitleType(String valrepLandimpPropdescTitleType) {
        this.valrepLandimpPropdescTitleType = valrepLandimpPropdescTitleType;
    }

    public String getValrepLandimpTaxDecClassification() {
        return valrepLandimpTaxDecClassification;
    }

    public void setValrepLandimpTaxDecClassification(String valrepLandimpTaxDecClassification) {
        this.valrepLandimpTaxDecClassification = valrepLandimpTaxDecClassification;
    }

    public String getValrepLandimpTaxDecDeclarant() {
        return valrepLandimpTaxDecDeclarant;
    }

    public void setValrepLandimpTaxDecDeclarant(String valrepLandimpTaxDecDeclarant) {
        this.valrepLandimpTaxDecDeclarant = valrepLandimpTaxDecDeclarant;
    }

    public String getValrepLandimpTaxDecImp() {
        return valrepLandimpTaxDecImp;
    }

    public void setValrepLandimpTaxDecImp(String valrepLandimpTaxDecImp) {
        this.valrepLandimpTaxDecImp = valrepLandimpTaxDecImp;
    }

    public String getValrepLandimpTaxDecLot() {
        return valrepLandimpTaxDecLot;
    }

    public void setValrepLandimpTaxDecLot(String valrepLandimpTaxDecLot) {
        this.valrepLandimpTaxDecLot = valrepLandimpTaxDecLot;
    }

    public String getValrepLandimpTaxDecLotNo() {
        return valrepLandimpTaxDecLotNo;
    }

    public void setValrepLandimpTaxDecLotNo(String valrepLandimpTaxDecLotNo) {
        this.valrepLandimpTaxDecLotNo = valrepLandimpTaxDecLotNo;
    }

    public String getValrepTableAppLegalDescription() {
        return valrepTableAppLegalDescription;
    }

    public void setValrepTableAppLegalDescription(String valrepTableAppLegalDescription) {
        this.valrepTableAppLegalDescription = valrepTableAppLegalDescription;
    }

}