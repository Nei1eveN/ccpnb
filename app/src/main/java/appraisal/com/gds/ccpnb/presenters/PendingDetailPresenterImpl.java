package appraisal.com.gds.ccpnb.presenters;

import android.content.Context;

import appraisal.com.gds.ccpnb.adapters.JobAcceptanceDetailAdapter;

public class PendingDetailPresenterImpl implements PendingDetailPresenter, PendingDetailInteractor.DetailsListener {
    private PendingDetailPresenter.View view;
    private PendingDetailInteractor interactor;

    public PendingDetailPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new PendingDetailInteractorImpl(context);
    }

    @Override
    public void onStart(String record_id) {
        if (view != null) {
            view.showProgress("Fetching Details", "Please wait...");
        }
        interactor.getOfflineData(record_id, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestDetails(String record_id) {
        if (view != null) {
            view.showProgress("Fetching Details", "Please wait...");
        }
        interactor.getDetails(record_id, this);
    }

    @Override
    public void onDetailSuccess(JobAcceptanceDetailAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.setListDetail(adapter);
        }
    }

    @Override
    public void onDetailError(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
        }
    }
}
