package appraisal.com.gds.ccpnb.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import appraisal.com.gds.ccpnb.R;
import appraisal.com.gds.ccpnb.models.ReworkRecord;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ReworkAdapter extends RecyclerView.Adapter<ReworkAdapter.ReworkViewHolder> {
    private Context context;
    private List<ReworkRecord> jobAcceptanceRecords;

    public ReworkAdapter(Context context, List<ReworkRecord> jobAcceptanceRecords) {
        this.context = context;
        this.jobAcceptanceRecords = jobAcceptanceRecords;
    }

    static class ReworkViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvRecordName)
        TextView accountName;
        @BindView(R.id.tvApprType)
        TextView appraisalType;

        ReworkViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ReworkViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ReworkViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_job_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ReworkViewHolder holder, int position) {
        ReworkRecord record = jobAcceptanceRecords.get(position);

        holder.accountName.setText(String.format("%s, %s %s", record.getAppAccountLastName(), record.getAppAccountFirstName(), record.getAppAccountMiddleName()));
        holder.appraisalType.setText(record.getAppRequestAppraisal());
    }

    @Override
    public int getItemCount() {
        return jobAcceptanceRecords.size();
    }
}
