package appraisal.com.gds.ccpnb.presenters.splash;

import android.content.Context;
import android.content.Intent;

import appraisal.com.gds.ccpnb.AppraiserHomeActivity;
import appraisal.com.gds.ccpnb.LoginActivity;
import appraisal.com.gds.ccpnb.utils.SessionManager;

import static appraisal.com.gds.ccpnb.utils.Constants.KEY_USER_EMAIL;
import static appraisal.com.gds.ccpnb.utils.Constants.KEY_USER_ROLE;
import static appraisal.com.gds.ccpnb.utils.Constants.USER_APPRAISER;

class SplashInteractorImpl implements SplashInteractor {
    private Context context;

    SplashInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUser(FindUserListener listener) {
        SessionManager sessionManager = new SessionManager(context);
        Intent resultData;
        if (sessionManager.isLoggedIn()) {
            if (sessionManager.getUserRole().equals(sessionManager.getUserRole())) {
                resultData = new Intent(context, AppraiserHomeActivity.class);
                resultData.putExtra(KEY_USER_EMAIL, sessionManager.getUserEmail());
                resultData.putExtra(KEY_USER_ROLE, USER_APPRAISER);
                listener.onUserFound("Logged in as "+sessionManager.getUserEmail(), resultData);
            }
        } else {
            resultData = new Intent(context, LoginActivity.class);
            listener.onUserNotFound("Heading To Login Page", resultData);
        }
    }
}
