package appraisal.com.gds.ccpnb;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Spinner;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import appraisal.com.gds.ccpnb.presenters.login.LoginPresenter;
import appraisal.com.gds.ccpnb.presenters.login.LoginPresenterImpl;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View {

    @BindView(R.id.txEmail) TextInputLayout emailWrapper;
    @BindView(R.id.txPassword) TextInputLayout passwordWrapper;

    @BindView(R.id.etEmail) TextInputEditText email;
    @BindView(R.id.etPassword) TextInputEditText password;

    @BindView(R.id.spUserType) Spinner spUserType;

    @BindView(R.id.btnLogin) Button btnLogin;

    ProgressDialog progressDialog;

    LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);

        presenter = new LoginPresenterImpl(this, this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Please Login");
    }

    @OnClick(R.id.btnLogin)
    void submitCredentials() {
        presenter.submitLoginCredentials(Objects.requireNonNull(email.getText()).toString(), Objects.requireNonNull(password.getText()).toString(), spUserType.getSelectedItem().toString());
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showErrorDialog(String title, String message) {
        new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("DISMISS", (dialogInterface, i) -> dialogInterface.dismiss()).create().show();
    }

    @Override
    public void showUserPageDialog(String title, String message, Intent userData) {
        new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("DISMISS", (dialogInterface, i) -> {
            dialogInterface.dismiss();
            startActivity(userData);
            finish();
        }).create().show();
    }
}
