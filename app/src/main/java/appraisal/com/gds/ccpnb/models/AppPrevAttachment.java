package appraisal.com.gds.ccpnb.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;

public class AppPrevAttachment {

    @SerializedName("app_prev_attachment_appraisal_type")
    @Expose
    private String appPrevAttachmentAppraisalType;
    @SerializedName("app_prev_attachment_date")
    @Expose
    private String appPrevAttachmentDate;
    @SerializedName("app_prev_attachment_file")
    @Expose
    private String appPrevAttachmentFile;
    @SerializedName("app_prev_attachment_filename")
    @Expose
    private String appPrevAttachmentFilename;

    public String getAppPrevAttachmentAppraisalType() {
        return appPrevAttachmentAppraisalType;
    }

    public void setAppPrevAttachmentAppraisalType(String appPrevAttachmentAppraisalType) {
        this.appPrevAttachmentAppraisalType = appPrevAttachmentAppraisalType;
    }

    public String getAppPrevAttachmentDate() {
        return appPrevAttachmentDate;
    }

    public void setAppPrevAttachmentDate(String appPrevAttachmentDate) {
        this.appPrevAttachmentDate = appPrevAttachmentDate;
    }

    public String getAppPrevAttachmentFile() {
        return appPrevAttachmentFile;
    }

    public void setAppPrevAttachmentFile(String appPrevAttachmentFile) {
        this.appPrevAttachmentFile = appPrevAttachmentFile;
    }

    public String getAppPrevAttachmentFilename() {
        return appPrevAttachmentFilename;
    }

    public void setAppPrevAttachmentFilename(String appPrevAttachmentFilename) {
        this.appPrevAttachmentFilename = appPrevAttachmentFilename;
    }

    @SerializedName("app_has_tvr")
    @Expose
    private String appHasTvr;
    @SerializedName("app_nature_appraisal")
    @Expose
    private String appNatureAppraisal;
    @SerializedName("app_purpose_appraisal")
    @Expose
    private String appPurposeAppraisal;
    @SerializedName("app_appraisal_procedure")
    @Expose
    private String appAppraisalProcedure;

    public String getAppHasTvr() {
        return appHasTvr;
    }

    public void setAppHasTvr(String appHasTvr) {
        this.appHasTvr = appHasTvr;
    }

    public String getAppNatureAppraisal() {
        return appNatureAppraisal;
    }

    public void setAppNatureAppraisal(String appNatureAppraisal) {
        this.appNatureAppraisal = appNatureAppraisal;
    }

    public String getAppPurposeAppraisal() {
        return appPurposeAppraisal;
    }

    public void setAppPurposeAppraisal(String appPurposeAppraisal) {
        this.appPurposeAppraisal = appPurposeAppraisal;
    }

    public String getAppAppraisalProcedure() {
        return appAppraisalProcedure;
    }

    public void setAppAppraisalProcedure(String appAppraisalProcedure) {
        this.appAppraisalProcedure = appAppraisalProcedure;
    }

    /**PREV ATTACHMENT**/

    @SerializedName("app_accept_job")
    @Expose
    private String appAcceptJob;
    @SerializedName("app_account_first_name")
    @Expose
    private String appAccountFirstName;
    @SerializedName("app_account_last_name")
    @Expose
    private String appAccountLastName;
    @SerializedName("app_account_middle_name")
    @Expose
    private String appAccountMiddleName;
    @SerializedName("app_borrower_type")
    @Expose
    private String appBorrowerType;
    @SerializedName("app_cross_reference_first_name")
    @Expose
    private String appCrossReferenceFirstName;
    @SerializedName("app_cross_reference_last_name")
    @Expose
    private String appCrossReferenceLastName;
    @SerializedName("app_cross_reference_middle_name")
    @Expose
    private String appCrossReferenceMiddleName;
    @SerializedName("app_cross_reference_relation")
    @Expose
    private String appCrossReferenceRelation;
    @SerializedName("app_daterequested_complete")
    @Expose
    private String appDaterequestedComplete;
    @SerializedName("app_daterequested_complete_dateTime")
    @Expose
    private String appDaterequestedCompleteDateTime;
    @SerializedName("app_daterequested_day")
    @Expose
    private String appDaterequestedDay;
    @SerializedName("app_daterequested_month")
    @Expose
    private String appDaterequestedMonth;
    @SerializedName("app_daterequested_year")
    @Expose
    private Integer appDaterequestedYear;
    @SerializedName("app_declined_reason")
    @Expose
    private String appDeclinedReason;
    @SerializedName("app_desired_loan_amount")
    @Expose
    private Integer appDesiredLoanAmount;
    @SerializedName("app_job_acceptance_remarks")
    @Expose
    private String appJobAcceptanceRemarks;
    @SerializedName("app_main_id")
    @Expose
    private String appMainId;
    @SerializedName("app_manager_code")
    @Expose
    private String appManagerCode;
    @SerializedName("app_manager_username")
    @Expose
    private String appManagerUsername;
    @SerializedName("app_occupancy_status")
    @Expose
    private String appOccupancyStatus;
    @SerializedName("app_osp_agency_name")
    @Expose
    private String appOspAgencyName;
    @SerializedName("app_osp_agent_code")
    @Expose
    private String appOspAgentCode;
    @SerializedName("app_osp_agent_name")
    @Expose
    private String appOspAgentName;
    @SerializedName("app_osp_agent_username")
    @Expose
    private String appOspAgentUsername;
    @SerializedName("app_osp_flag")
    @Expose
    private String appOspFlag;
    @SerializedName("app_prev_attachment")
    @Expose
    private RealmList<AppPrevAttachment> appPrevAttachment = new RealmList<>();
    @SerializedName("app_property_occupants")
    @Expose
    private String appPropertyOccupants;
    @SerializedName("app_request_remarks")
    @Expose
    private String appRequestRemarks;
    @SerializedName("app_requesting_party")
    @Expose
    private String appRequestingParty;
    @SerializedName("app_requesting_party_code")
    @Expose
    private String appRequestingPartyCode;
    @SerializedName("app_requestor")
    @Expose
    private String appRequestor;
    @SerializedName("app_requestor_username")
    @Expose
    private String appRequestorUsername;
    @SerializedName("app_return_to_requestor")
    @Expose
    private String appReturnToRequestor;
    @SerializedName("app_return_to_requestor_remarks")
    @Expose
    private String appReturnToRequestorRemarks;
    @SerializedName("app_simul_type")
    @Expose
    private String appSimulType;
    @SerializedName("app_source_assignment")
    @Expose
    private String appSourceAssignment;
    @SerializedName("app_to_close")
    @Expose
    private String appToClose;
    @SerializedName("app_type_of_loan")
    @Expose
    private String appTypeOfLoan;
    @SerializedName("application_status")
    @Expose
    private String applicationStatus;
    @SerializedName("paf_landimp_developer_name")
    @Expose
    private String pafLandimpDeveloperName;
    @SerializedName("paf_landimp_project_name")
    @Expose
    private String pafLandimpProjectName;

    public String getAppAcceptJob() {
        return appAcceptJob;
    }

    public void setAppAcceptJob(String appAcceptJob) {
        this.appAcceptJob = appAcceptJob;
    }

    public String getAppAccountFirstName() {
        return appAccountFirstName;
    }

    public void setAppAccountFirstName(String appAccountFirstName) {
        this.appAccountFirstName = appAccountFirstName;
    }

    public String getAppAccountLastName() {
        return appAccountLastName;
    }

    public void setAppAccountLastName(String appAccountLastName) {
        this.appAccountLastName = appAccountLastName;
    }

    public String getAppAccountMiddleName() {
        return appAccountMiddleName;
    }

    public void setAppAccountMiddleName(String appAccountMiddleName) {
        this.appAccountMiddleName = appAccountMiddleName;
    }

    public String getAppBorrowerType() {
        return appBorrowerType;
    }

    public void setAppBorrowerType(String appBorrowerType) {
        this.appBorrowerType = appBorrowerType;
    }

    public String getAppCrossReferenceFirstName() {
        return appCrossReferenceFirstName;
    }

    public void setAppCrossReferenceFirstName(String appCrossReferenceFirstName) {
        this.appCrossReferenceFirstName = appCrossReferenceFirstName;
    }

    public String getAppCrossReferenceLastName() {
        return appCrossReferenceLastName;
    }

    public void setAppCrossReferenceLastName(String appCrossReferenceLastName) {
        this.appCrossReferenceLastName = appCrossReferenceLastName;
    }

    public String getAppCrossReferenceMiddleName() {
        return appCrossReferenceMiddleName;
    }

    public void setAppCrossReferenceMiddleName(String appCrossReferenceMiddleName) {
        this.appCrossReferenceMiddleName = appCrossReferenceMiddleName;
    }

    public String getAppCrossReferenceRelation() {
        return appCrossReferenceRelation;
    }

    public void setAppCrossReferenceRelation(String appCrossReferenceRelation) {
        this.appCrossReferenceRelation = appCrossReferenceRelation;
    }

    public String getAppDaterequestedComplete() {
        return appDaterequestedComplete;
    }

    public void setAppDaterequestedComplete(String appDaterequestedComplete) {
        this.appDaterequestedComplete = appDaterequestedComplete;
    }

    public String getAppDaterequestedCompleteDateTime() {
        return appDaterequestedCompleteDateTime;
    }

    public void setAppDaterequestedCompleteDateTime(String appDaterequestedCompleteDateTime) {
        this.appDaterequestedCompleteDateTime = appDaterequestedCompleteDateTime;
    }

    public String getAppDaterequestedDay() {
        return appDaterequestedDay;
    }

    public void setAppDaterequestedDay(String appDaterequestedDay) {
        this.appDaterequestedDay = appDaterequestedDay;
    }

    public String getAppDaterequestedMonth() {
        return appDaterequestedMonth;
    }

    public void setAppDaterequestedMonth(String appDaterequestedMonth) {
        this.appDaterequestedMonth = appDaterequestedMonth;
    }

    public Integer getAppDaterequestedYear() {
        return appDaterequestedYear;
    }

    public void setAppDaterequestedYear(Integer appDaterequestedYear) {
        this.appDaterequestedYear = appDaterequestedYear;
    }

    public String getAppDeclinedReason() {
        return appDeclinedReason;
    }

    public void setAppDeclinedReason(String appDeclinedReason) {
        this.appDeclinedReason = appDeclinedReason;
    }

    public Integer getAppDesiredLoanAmount() {
        return appDesiredLoanAmount;
    }

    public void setAppDesiredLoanAmount(Integer appDesiredLoanAmount) {
        this.appDesiredLoanAmount = appDesiredLoanAmount;
    }

    public String getAppJobAcceptanceRemarks() {
        return appJobAcceptanceRemarks;
    }

    public void setAppJobAcceptanceRemarks(String appJobAcceptanceRemarks) {
        this.appJobAcceptanceRemarks = appJobAcceptanceRemarks;
    }

    public String getAppMainId() {
        return appMainId;
    }

    public void setAppMainId(String appMainId) {
        this.appMainId = appMainId;
    }

    public String getAppManagerCode() {
        return appManagerCode;
    }

    public void setAppManagerCode(String appManagerCode) {
        this.appManagerCode = appManagerCode;
    }

    public String getAppManagerUsername() {
        return appManagerUsername;
    }

    public void setAppManagerUsername(String appManagerUsername) {
        this.appManagerUsername = appManagerUsername;
    }

    public String getAppOccupancyStatus() {
        return appOccupancyStatus;
    }

    public void setAppOccupancyStatus(String appOccupancyStatus) {
        this.appOccupancyStatus = appOccupancyStatus;
    }

    public String getAppOspAgencyName() {
        return appOspAgencyName;
    }

    public void setAppOspAgencyName(String appOspAgencyName) {
        this.appOspAgencyName = appOspAgencyName;
    }

    public String getAppOspAgentCode() {
        return appOspAgentCode;
    }

    public void setAppOspAgentCode(String appOspAgentCode) {
        this.appOspAgentCode = appOspAgentCode;
    }

    public String getAppOspAgentName() {
        return appOspAgentName;
    }

    public void setAppOspAgentName(String appOspAgentName) {
        this.appOspAgentName = appOspAgentName;
    }

    public String getAppOspAgentUsername() {
        return appOspAgentUsername;
    }

    public void setAppOspAgentUsername(String appOspAgentUsername) {
        this.appOspAgentUsername = appOspAgentUsername;
    }

    public String getAppOspFlag() {
        return appOspFlag;
    }

    public void setAppOspFlag(String appOspFlag) {
        this.appOspFlag = appOspFlag;
    }

    public RealmList<AppPrevAttachment> getAppPrevAttachment() {
        return appPrevAttachment;
    }

    public void setAppPrevAttachment(RealmList<AppPrevAttachment> appPrevAttachment) {
        this.appPrevAttachment = appPrevAttachment;
    }

    public String getAppPropertyOccupants() {
        return appPropertyOccupants;
    }

    public void setAppPropertyOccupants(String appPropertyOccupants) {
        this.appPropertyOccupants = appPropertyOccupants;
    }

    public String getAppRequestRemarks() {
        return appRequestRemarks;
    }

    public void setAppRequestRemarks(String appRequestRemarks) {
        this.appRequestRemarks = appRequestRemarks;
    }

    public String getAppRequestingParty() {
        return appRequestingParty;
    }

    public void setAppRequestingParty(String appRequestingParty) {
        this.appRequestingParty = appRequestingParty;
    }

    public String getAppRequestingPartyCode() {
        return appRequestingPartyCode;
    }

    public void setAppRequestingPartyCode(String appRequestingPartyCode) {
        this.appRequestingPartyCode = appRequestingPartyCode;
    }

    public String getAppRequestor() {
        return appRequestor;
    }

    public void setAppRequestor(String appRequestor) {
        this.appRequestor = appRequestor;
    }

    public String getAppRequestorUsername() {
        return appRequestorUsername;
    }

    public void setAppRequestorUsername(String appRequestorUsername) {
        this.appRequestorUsername = appRequestorUsername;
    }

    public String getAppReturnToRequestor() {
        return appReturnToRequestor;
    }

    public void setAppReturnToRequestor(String appReturnToRequestor) {
        this.appReturnToRequestor = appReturnToRequestor;
    }

    public String getAppReturnToRequestorRemarks() {
        return appReturnToRequestorRemarks;
    }

    public void setAppReturnToRequestorRemarks(String appReturnToRequestorRemarks) {
        this.appReturnToRequestorRemarks = appReturnToRequestorRemarks;
    }

    public String getAppSimulType() {
        return appSimulType;
    }

    public void setAppSimulType(String appSimulType) {
        this.appSimulType = appSimulType;
    }

    public String getAppSourceAssignment() {
        return appSourceAssignment;
    }

    public void setAppSourceAssignment(String appSourceAssignment) {
        this.appSourceAssignment = appSourceAssignment;
    }

    public String getAppToClose() {
        return appToClose;
    }

    public void setAppToClose(String appToClose) {
        this.appToClose = appToClose;
    }

    public String getAppTypeOfLoan() {
        return appTypeOfLoan;
    }

    public void setAppTypeOfLoan(String appTypeOfLoan) {
        this.appTypeOfLoan = appTypeOfLoan;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public String getPafLandimpDeveloperName() {
        return pafLandimpDeveloperName;
    }

    public void setPafLandimpDeveloperName(String pafLandimpDeveloperName) {
        this.pafLandimpDeveloperName = pafLandimpDeveloperName;
    }

    public String getPafLandimpProjectName() {
        return pafLandimpProjectName;
    }

    public void setPafLandimpProjectName(String pafLandimpProjectName) {
        this.pafLandimpProjectName = pafLandimpProjectName;
    }

}