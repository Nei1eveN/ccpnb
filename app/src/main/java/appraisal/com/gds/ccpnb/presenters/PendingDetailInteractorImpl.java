package appraisal.com.gds.ccpnb.presenters;

import android.content.Context;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import appraisal.com.gds.ccpnb.adapters.JobAcceptanceDetailAdapter;
import appraisal.com.gds.ccpnb.models.AppCollateralAddress;
import appraisal.com.gds.ccpnb.models.AppContactPerson;
import appraisal.com.gds.ccpnb.models.AppPrevAttachment;
import appraisal.com.gds.ccpnb.models.PendingJobDetails;
import appraisal.com.gds.ccpnb.models.ValrepLandimpImpDetail;
import appraisal.com.gds.ccpnb.models.ValrepLandimpLotDetail;
import io.realm.Realm;
import io.realm.RealmResults;

import static appraisal.com.gds.ccpnb.utils.Constants.APPLICATION_STATUS_HEADER;
import static appraisal.com.gds.ccpnb.utils.Constants.AUTH_TOKEN_VALUE;
import static appraisal.com.gds.ccpnb.utils.Constants.PNB_URL_VIEW_RECORD;
import static appraisal.com.gds.ccpnb.utils.Constants.QUERY_PARAM;
import static appraisal.com.gds.ccpnb.utils.Constants.RECORD;
import static appraisal.com.gds.ccpnb.utils.Constants.RECORD_ID;
import static appraisal.com.gds.ccpnb.utils.Constants.STATUS_JOB_ACCEPTED;
import static appraisal.com.gds.ccpnb.utils.Constants.SYSTEM_RECORD_ID;
import static appraisal.com.gds.ccpnb.utils.Constants.USER_AUTH_TOKEN;
import static appraisal.com.gds.ccpnb.utils.Constants.isNetworkAvailable;

class PendingDetailInteractorImpl implements PendingDetailInteractor {
    private Context context;
    private Realm realm = Realm.getDefaultInstance();

    PendingDetailInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getDetails(String record_id, DetailsListener listener) {
        if (!isNetworkAvailable(context)) {
            getOfflineData(record_id, listener);
        } else {
            JSONObject queryObject = new JSONObject();
            JSONObject dataObject = new JSONObject();
            JSONObject recordParamObject = new JSONObject();
            try {
                queryObject.put(SYSTEM_RECORD_ID, record_id);
                dataObject.put(APPLICATION_STATUS_HEADER, STATUS_JOB_ACCEPTED);
                recordParamObject.put(RECORD, dataObject);
            } catch (JSONException e) {
                listener.onDetailError("JSON Error:\n" + e.getLocalizedMessage());
            }

            Log.d("interactor--RecordID", record_id);
            Log.d("interactor--JSON", String.valueOf(queryObject));

            AndroidNetworking.post(PNB_URL_VIEW_RECORD)
                    .addBodyParameter(USER_AUTH_TOKEN, AUTH_TOKEN_VALUE)
                    .addBodyParameter(QUERY_PARAM, String.valueOf(queryObject))
//                    .addBodyParameter(DATA_PARAM, String.valueOf(recordParamObject))
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("interactor--JSONResp", String.valueOf(response));
                            try {
                                List<PendingJobDetails> jobDetails = new ArrayList<>();

                                AppPrevAttachment attachment = new AppPrevAttachment();

                                /**ACCOUNT NAME**/
                                attachment.setAppAccountFirstName(response.getString("app_account_first_name"));
                                attachment.setAppAccountMiddleName(response.getString("app_account_middle_name"));
                                attachment.setAppAccountLastName(response.getString("app_account_last_name"));

                                PendingJobDetails details0 = new PendingJobDetails();
                                details0.setRecord_id(record_id);
                                details0.setTitle("Account Name");
                                details0.setContent(attachment.getAppAccountLastName() + ", " + attachment.getAppAccountFirstName() + " " + attachment.getAppAccountMiddleName());

                                /**BORROWER TYPE**/
                                attachment.setAppBorrowerType(response.getString("app_borrower_type"));

                                PendingJobDetails details1 = new PendingJobDetails();
                                details1.setRecord_id(record_id);
                                details1.setTitle("Borrower Type");
                                details1.setContent(attachment.getAppBorrowerType());

                                /**CROSS REFERENCE**/
                                attachment.setAppCrossReferenceFirstName(response.getString("app_cross_reference_first_name"));
                                attachment.setAppCrossReferenceMiddleName(response.getString("app_cross_reference_middle_name"));
                                attachment.setAppCrossReferenceLastName(response.getString("app_cross_reference_last_name"));
                                attachment.setAppCrossReferenceRelation(response.getString("app_cross_reference_relation"));

                                PendingJobDetails details2 = new PendingJobDetails();
                                details2.setRecord_id(record_id);
                                details2.setTitle("Cross Reference");
                                details2.setContent(attachment.getAppCrossReferenceLastName() + ", " + attachment.getAppCrossReferenceFirstName() + " " + attachment.getAppCrossReferenceMiddleName());

                                PendingJobDetails details3 = new PendingJobDetails();
                                details3.setRecord_id(record_id);
                                details3.setTitle("Relation");
                                if (attachment.getAppCrossReferenceRelation().isEmpty()) {
                                    details3.setContent("(not set)");
                                } else {
                                    details3.setContent(attachment.getAppCrossReferenceRelation());
                                }

                                /**DEVELOPER NAME**/
                                attachment.setPafLandimpDeveloperName(response.getString("paf_landimp_developer_name"));

                                PendingJobDetails details4 = new PendingJobDetails();
                                details4.setRecord_id(record_id);
                                details4.setTitle("Developer Name");
                                if (attachment.getPafLandimpDeveloperName().isEmpty()) {
                                    details4.setContent("(not set)");
                                } else {
                                    details4.setContent(attachment.getPafLandimpDeveloperName());
                                }

                                /**PROJECT NAME**/
                                attachment.setPafLandimpProjectName(response.getString("paf_landimp_project_name"));
                                PendingJobDetails details5 = new PendingJobDetails();
                                details5.setRecord_id(record_id);
                                details5.setTitle("Project Name");

                                if (attachment.getPafLandimpProjectName().isEmpty()) {
                                    details5.setContent("(not set)");
                                } else {
                                    details5.setContent(attachment.getPafLandimpProjectName());
                                }

                                /**REQUESTING UNIT**/
                                attachment.setAppRequestingParty(response.getString("app_requesting_party"));
                                attachment.setAppRequestor(response.getString("app_requestor"));
                                attachment.setAppSourceAssignment(response.getString("app_source_assignment"));

                                PendingJobDetails details6 = new PendingJobDetails();
                                details6.setRecord_id(record_id);
                                details6.setTitle("Requesting Unit");
                                details6.setContent(attachment.getAppRequestingParty());

                                PendingJobDetails details7 = new PendingJobDetails();
                                details7.setRecord_id(record_id);
                                details7.setTitle("Requesting Officer");
                                details7.setContent(attachment.getAppRequestor());

                                PendingJobDetails details8 = new PendingJobDetails();
                                details8.setRecord_id(record_id);
                                details8.setTitle("Source");
                                details8.setContent(attachment.getAppSourceAssignment());

                                /**APPRAISAL REQUEST ARRAY**/
                                JSONArray appraisalRequest = response.getJSONArray("appraisal_request");

                                /**APPRAISAL REQUEST OBJECT**/
                                for (int i = 0; i < appraisalRequest.length(); i++) {
                                    JSONObject appraisalObject = appraisalRequest.getJSONObject(i);

                                    attachment.setAppHasTvr(appraisalObject.getString("app_has_tvr"));
                                    PendingJobDetails details9 = new PendingJobDetails();
                                    details9.setRecord_id(record_id);
                                    details9.setTitle("Includes TVR");
                                    if (attachment.getAppHasTvr().equals("true")) {
                                        details9.setContent("Yes");
                                    } else {
                                        details9.setContent("No");
                                    }

                                    attachment.setAppNatureAppraisal(appraisalObject.getString("app_nature_appraisal"));
                                    PendingJobDetails details10 = new PendingJobDetails();
                                    details10.setRecord_id(record_id);
                                    details10.setTitle("Appraisal Type");
                                    details10.setContent(attachment.getAppNatureAppraisal());

                                    attachment.setAppPurposeAppraisal(appraisalObject.getString("app_purpose_appraisal"));
                                    PendingJobDetails details11 = new PendingJobDetails();
                                    details11.setRecord_id(record_id);
                                    details11.setTitle("Purpose of Appraisal");
                                    details11.setContent(attachment.getAppPurposeAppraisal());

                                    attachment.setAppAppraisalProcedure(appraisalObject.getString("app_appraisal_procedure"));
                                    PendingJobDetails details12 = new PendingJobDetails();
                                    details12.setRecord_id(record_id);
                                    details12.setTitle("Appraisal Procedure");
                                    details12.setContent(attachment.getAppAppraisalProcedure());

                                    /**REQUESTED DATE**/
                                    attachment.setAppDaterequestedDay(response.getString("app_daterequested_day"));
                                    attachment.setAppDaterequestedMonth(response.getString("app_daterequested_month"));
                                    attachment.setAppDaterequestedYear(response.getInt("app_daterequested_year"));

                                    PendingJobDetails details13 = new PendingJobDetails();
                                    details13.setRecord_id(record_id);
                                    details13.setTitle("Date Requested");
                                    details13.setContent(attachment.getAppDaterequestedMonth() + "/" + attachment.getAppDaterequestedDay() + "/" + attachment.getAppDaterequestedYear());

                                    /**REQUEST REMARKS**/
                                    attachment.setAppRequestRemarks(appraisalObject.getString("app_request_remarks"));
                                    PendingJobDetails details14 = new PendingJobDetails();
                                    details14.setRecord_id(record_id);
                                    details14.setTitle("Request Remarks / Special Instructions");
                                    if (attachment.getAppRequestRemarks().isEmpty()) {
                                        details14.setContent("(not set)");
                                    } else {
                                        details14.setContent(attachment.getAppRequestRemarks());
                                    }

                                    jobDetails.add(details0);
                                    jobDetails.add(details1);
                                    jobDetails.add(details2);
                                    jobDetails.add(details3);
                                    jobDetails.add(details4);
                                    jobDetails.add(details5);
                                    jobDetails.add(details6);
                                    jobDetails.add(details7);
                                    jobDetails.add(details8);
                                    jobDetails.add(details9);
                                    jobDetails.add(details10);
                                    jobDetails.add(details11);
                                    jobDetails.add(details12);
                                    jobDetails.add(details13);
                                    jobDetails.add(details14);

                                    if (i == (appraisalRequest.length() - 1)) {
                                        /**CONTACTS**/
                                        JSONArray contactPersonArray = appraisalObject.getJSONArray("app_contact_person");
                                        for (int j = 0; j < contactPersonArray.length(); j++) {
                                            JSONObject contactPersonObject = contactPersonArray.getJSONObject(j);
                                            AppContactPerson person = new AppContactPerson();
                                            person.setAppContactPersonIndex(contactPersonObject.getString("app_contact_person_index"));
                                            person.setAppContactPersonName(contactPersonObject.getString("app_contact_person_name"));
                                            person.setAppContactPersonType(contactPersonObject.getString("app_contact_person_type"));
                                            person.setAppContactType(contactPersonObject.getString("app_contact_type"));
                                            person.setAppEmailAdd(contactPersonObject.getString("app_email_add"));
                                            person.setAppMobileNo(contactPersonObject.getString("app_mobile_no"));
                                            person.setAppMobilePrefix(contactPersonObject.getString("app_mobile_prefix"));
                                            person.setAppTelephoneArea(contactPersonObject.getString("app_telephone_area"));
                                            person.setAppTelephoneNo(contactPersonObject.getString("app_telephone_no"));

                                            PendingJobDetails details15 = new PendingJobDetails();
                                            details15.setRecord_id(record_id);
                                            details15.setTitle("Contact "+String.valueOf(j+1));

                                            details15.setContent("Index: " +person.getAppContactPersonIndex()+"\n"
                                                    +"Name: "+person.getAppContactPersonName()+"\n"
                                                    +"Contact Type: "+person.getAppContactPersonType()+"\n"
                                                    +"App Contact Type: "+person.getAppContactType()+"\n"
                                                    +"Email Address: "+person.getAppEmailAdd()+"\n"
                                                    +"Mobile No.: "+person.getAppMobileNo()+"\n"
                                                    +"Mobile Prefix: "+person.getAppMobilePrefix()+"\n"
                                                    +"Telephone Area: "+person.getAppTelephoneArea()+"\n"
                                                    +"Telephone No.: "+person.getAppTelephoneNo());

                                            jobDetails.add(details15);
                                        }

                                        /**LAND TITLE ARRAY**/
                                        JSONArray landTitleArray = response.getJSONArray("valrep_landimp_lot_details");
                                        for (int k = 0; k < landTitleArray.length(); k++) {
                                            JSONObject landTitleObject = landTitleArray.getJSONObject(k);
                                            ValrepLandimpLotDetail lotDetail = new ValrepLandimpLotDetail();
                                            lotDetail.setValrepLandimpPropdescIndex(landTitleObject.getString("valrep_landimp_propdesc_index"));
                                            lotDetail.setValrepLandimpPropdescTitleType(landTitleObject.getString("valrep_landimp_propdesc_title_type"));
                                            lotDetail.setValrepLandimpPropdescTctNo(landTitleObject.getString("valrep_landimp_propdesc_tct_no"));
                                            lotDetail.setValrepLandimpPropdescLot(landTitleObject.getString("valrep_landimp_propdesc_lot"));
                                            lotDetail.setValrepLandimpPropdescBlock(landTitleObject.getString("valrep_landimp_propdesc_block"));
                                            lotDetail.setValrepLandimpPropdescSurveyNos(landTitleObject.getString("valrep_landimp_propdesc_survey_nos"));
                                            lotDetail.setValrepLandimpPropdescArea(landTitleObject.getString("valrep_landimp_propdesc_area"));
                                            lotDetail.setValrepLandimpPropdescRegistryDateDay(landTitleObject.getString("valrep_landimp_propdesc_registry_date_day"));
                                            lotDetail.setValrepLandimpPropdescRegistryDateMonth(landTitleObject.getString("valrep_landimp_propdesc_registry_date_month"));
                                            lotDetail.setValrepLandimpPropdescRegistryDateYear(landTitleObject.getString("valrep_landimp_propdesc_registry_date_year"));
                                            lotDetail.setValrepLandimpPropdescRegisteredOwner(landTitleObject.getString("valrep_landimp_propdesc_registered_owner"));
                                            lotDetail.setValrepLandimpPropdescRegistryOfDeeds(landTitleObject.getString("valrep_landimp_propdesc_registry_of_deeds"));


                                            PendingJobDetails details16 = new PendingJobDetails();
                                            details16.setRecord_id(record_id);
                                            details16.setTitle("Title Details "+String.valueOf(k+1));
                                            details16.setContent
                                                    (
                                                            "RAS Ledger Index: "+lotDetail.getValrepLandimpPropdescIndex()+"\n"+
                                                                    "Title Type: "+lotDetail.getValrepLandimpPropdescTitleType()+"\n"+
                                                                    "Title No.: "+lotDetail.getValrepLandimpPropdescTctNo()+"\n"+
                                                                    "Lot No.: "+lotDetail.getValrepLandimpPropdescLot()+"\n"+
                                                                    "Block: "+lotDetail.getValrepLandimpPropdescBlock()+"\n"+
                                                                    "Survey No.: "+lotDetail.getValrepLandimpPropdescSurveyNos()+"\n"+
                                                                    "Area No.: "+lotDetail.getValrepLandimpPropdescArea()+"\n"+
                                                                    "Date of Registry: "+lotDetail.getValrepLandimpPropdescRegistryDateMonth()+"/"+lotDetail.getValrepLandimpPropdescRegistryDateDay()+"/"+lotDetail.getValrepLandimpPropdescRegistryDateYear()+"\n"+
                                                                    "Registered Owner(s):\n "+lotDetail.getValrepLandimpPropdescRegisteredOwner()+"\n"+
                                                                    "Registry of Deeds:\n "+lotDetail.getValrepLandimpPropdescRegistryOfDeeds()
                                                    );

                                            jobDetails.add(details16);
                                        }

                                        /**LAND IMP ARRAY**/
                                        JSONArray landImpArray = response.getJSONArray("valrep_landimp_imp_details");
                                        for (int k = 0; k < landImpArray.length(); k++) {
                                            JSONObject landImpObject = landImpArray.getJSONObject(k);
                                            ValrepLandimpImpDetail impDetail = new ValrepLandimpImpDetail();
                                            impDetail.setValrepLandimpImpsummary1Index(landImpObject.getString("valrep_landimp_impsummary1_index"));
                                            impDetail.setValrepLandimpImpsummary1BuildingDesc(landImpObject.getString("valrep_landimp_impsummary1_building_desc"));

                                            PendingJobDetails details17 = new PendingJobDetails();
                                            details17.setRecord_id(record_id);
                                            details17.setTitle("Improvement Details "+String.valueOf(k+1));
                                            details17.setContent
                                                    (
                                                            "RAS Ledger Index: "+impDetail.getValrepLandimpImpsummary1Index()+"\n"+
                                                                    "Building Description: "+impDetail.getValrepLandimpImpsummary1BuildingDesc()
                                                    );

                                            jobDetails.add(details17);
                                        }

                                        /**ADDRESS ARRAY**/
                                        JSONArray collateralAddressArray = appraisalObject.getJSONArray("app_collateral_address");
                                        for (int j = 0; j < collateralAddressArray.length(); j++) {
                                            JSONObject collateralObject = collateralAddressArray.getJSONObject(j);
                                            AppCollateralAddress collateralAddress = new AppCollateralAddress();
                                            collateralAddress.setAppBlockNo(collateralObject.getString("app_block_no"));
                                            collateralAddress.setAppDistrict(collateralObject.getString("app_district"));
                                            collateralAddress.setAppStreetName(collateralObject.getString("app_street_name"));
                                            collateralAddress.setAppStreetNo(collateralObject.getInt("app_street_no"));
                                            collateralAddress.setAppUnitNo(collateralObject.getString("app_unit_no"));
                                            collateralAddress.setAppVillage(collateralObject.getString("app_village"));
                                            collateralAddress.setAppCity(collateralObject.getString("app_city"));
                                            collateralAddress.setAppProvince(collateralObject.getString("app_province"));
                                            collateralAddress.setAppRegion(collateralObject.getString("app_region"));
                                            collateralAddress.setAppCountry(collateralObject.getString("app_country"));

                                            PendingJobDetails details18 = new PendingJobDetails();
                                            details18.setRecord_id(record_id);
                                            details18.setTitle("Address");
                                            details18.setContent
                                                    (
                                                            "Block No.: "+collateralAddress.getAppBlockNo()+"\n"+
                                                                    "District: "+collateralAddress.getAppDistrict()+"\n"+
                                                                    "Street Name: "+collateralAddress.getAppStreetName()+"\n"+
                                                                    "Street No.: "+collateralAddress.getAppStreetNo()+"\n"+
                                                                    "City: "+collateralAddress.getAppCity()+"\n"+
                                                                    "Province: "+collateralAddress.getAppProvince()+"\n"+
                                                                    "Region: "+collateralAddress.getAppRegion()+"\n"+
                                                                    "Country: "+collateralAddress.getAppCountry()
                                                    );

                                            jobDetails.add(details18);
                                        }

                                    }
                                }

                                realm.executeTransaction(realm -> {
                                    RealmResults<PendingJobDetails> pendingJobDetails = realm.where(PendingJobDetails.class).contains(RECORD_ID, record_id).findAll();
                                    if (pendingJobDetails != null) {
                                        pendingJobDetails.deleteAllFromRealm();
                                        Log.d("interactor--realm", "Realm data "+record_id+" must be deleted");
                                    }
                                    for (PendingJobDetails details : jobDetails) {
                                        PendingJobDetails jobDetails1 = realm.createObject(PendingJobDetails.class);

                                        jobDetails1.setRecord_id(details.getRecord_id());
                                        jobDetails1.setTitle(details.getTitle());
                                        jobDetails1.setContent(details.getContent());
                                    }
                                });

                                getOfflineData(record_id, listener);

                            } catch (JSONException e) {
                                listener.onDetailError("JSON ERROR:\n" + e.getLocalizedMessage());
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("interactor--JSONError", anError.getLocalizedMessage());
                        }
                    });

        }
    }

    @Override
    public void getOfflineData(String record_id, DetailsListener listener) {
        RealmResults<PendingJobDetails> detailsRealmResults = realm.where(PendingJobDetails.class).equalTo(RECORD_ID, record_id).findAll(); //.distinct("title","content")
        if (detailsRealmResults.isEmpty()) {
            if (isNetworkAvailable(context)) {
                getDetails(record_id, listener);
            } else {
                listener.onDetailError("Data Not Available. To retrieve the details, swipe down the screen to refresh.\n\nPlease check your network connection.");
            }
        } else {
            List<PendingJobDetails> details = realm.copyFromRealm(detailsRealmResults);
            JobAcceptanceDetailAdapter adapter = new JobAcceptanceDetailAdapter(context, details);
            listener.onDetailSuccess(adapter);
        }
    }
}
