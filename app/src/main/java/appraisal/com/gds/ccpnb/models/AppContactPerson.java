package appraisal.com.gds.ccpnb.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppContactPerson {

    @SerializedName("app_contact_person_index")
    @Expose
    private String appContactPersonIndex;
    @SerializedName("app_contact_person_name")
    @Expose
    private String appContactPersonName;
    @SerializedName("app_contact_person_type")
    @Expose
    private String appContactPersonType;
    @SerializedName("app_contact_type")
    @Expose
    private String appContactType;
    @SerializedName("app_email_add")
    @Expose
    private String appEmailAdd;
    @SerializedName("app_mobile_no")
    @Expose
    private String appMobileNo;
    @SerializedName("app_mobile_prefix")
    @Expose
    private String appMobilePrefix;
    @SerializedName("app_telephone_area")
    @Expose
    private String appTelephoneArea;
    @SerializedName("app_telephone_no")
    @Expose
    private String appTelephoneNo;

    public String getAppContactPersonIndex() {
        return appContactPersonIndex;
    }

    public void setAppContactPersonIndex(String appContactPersonIndex) {
        this.appContactPersonIndex = appContactPersonIndex;
    }

    public String getAppContactPersonName() {
        return appContactPersonName;
    }

    public void setAppContactPersonName(String appContactPersonName) {
        this.appContactPersonName = appContactPersonName;
    }

    public String getAppContactPersonType() {
        return appContactPersonType;
    }

    public void setAppContactPersonType(String appContactPersonType) {
        this.appContactPersonType = appContactPersonType;
    }

    public String getAppContactType() {
        return appContactType;
    }

    public void setAppContactType(String appContactType) {
        this.appContactType = appContactType;
    }

    public String getAppEmailAdd() {
        return appEmailAdd;
    }

    public void setAppEmailAdd(String appEmailAdd) {
        this.appEmailAdd = appEmailAdd;
    }

    public String getAppMobileNo() {
        return appMobileNo;
    }

    public void setAppMobileNo(String appMobileNo) {
        this.appMobileNo = appMobileNo;
    }

    public String getAppMobilePrefix() {
        return appMobilePrefix;
    }

    public void setAppMobilePrefix(String appMobilePrefix) {
        this.appMobilePrefix = appMobilePrefix;
    }

    public String getAppTelephoneArea() {
        return appTelephoneArea;
    }

    public void setAppTelephoneArea(String appTelephoneArea) {
        this.appTelephoneArea = appTelephoneArea;
    }

    public String getAppTelephoneNo() {
        return appTelephoneNo;
    }

    public void setAppTelephoneNo(String appTelephoneNo) {
        this.appTelephoneNo = appTelephoneNo;
    }

}
