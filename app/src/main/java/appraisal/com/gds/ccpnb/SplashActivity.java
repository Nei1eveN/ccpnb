package appraisal.com.gds.ccpnb;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import appraisal.com.gds.ccpnb.presenters.splash.SplashPresenter;
import appraisal.com.gds.ccpnb.presenters.splash.SplashPresenterImpl;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity implements SplashPresenter.View {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.tvCaptioner)
    TextView captionText;

    SplashPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        presenter = new SplashPresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setCaptionText(String message) {
        captionText.setText(message);
    }

    @Override
    public void goToDesignatedPage(Intent resultData) {
        startActivity(resultData);
        finish();
    }
}
